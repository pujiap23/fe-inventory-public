<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StockProductController;
use App\Http\Controllers\TransactionTypeController;
use App\Http\Controllers\TransactionStatusController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\UOMController;
use App\Http\Controllers\WarehouseController;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SalesBillingController;
use App\Http\Controllers\MutationProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::group(['prefix' => 'login'], function () {
    //Route::group(['prefix' => 'login'], function () {
        Route::get('/', [UserController::class, 'showLoginForm'])->name('auth-login');
        Route::post('/', [UserController::class, 'login'])->name('auth-login-process');
		Route::get('/logout', [UserController::class, 'logout'])->name('auth-logout');
    //});
});

Route::middleware(['custom_user_auth'])->group(function(){
	Route::group(['prefix' => 'home'], function () {
		Route::get('/', [HomeController::class, 'home'])->name('home');
	});
	
	Route::group(['prefix' => 'setting'], function () {
		Route::group(['prefix' => 'user'], function () {
			Route::get('/', [UserController::class, 'user_list_page'])->name('user_list_page');
		});
		Route::group(['prefix' => 'role'], function () {
			Route::get('/', [RoleController::class, 'role_list_page'])->name('role_list_page');
		});
		Route::group(['prefix' => 'menu'], function () {
			Route::get('/', [MenuController::class, 'menu_list_page'])->name('menu_list_page');
		});
		Route::group(['prefix' => 'tenant'], function () {
			Route::get('/', [TenantController::class, 'tenant_list_page'])->name('tenant_list_page');
		});
		Route::group(['prefix' => 'warehouse'], function () {
			Route::get('/', [WarehouseController::class, 'warehouse_list_page'])->name('warehouse_list_page');
		});
	});
	
	Route::group(['prefix' => 'master'], function () {
		Route::group(['prefix' => 'uom'], function () {
			Route::get('/', [UOMController::class, 'uom_list_page'])->name('uom_list_page');
		});
		Route::group(['prefix' => 'product-type'], function () {
			Route::get('/', [ProductTypeController::class, 'product_type_list_page'])->name('product_type_list_page');
		});
		Route::group(['prefix' => 'product'], function () {
			Route::get('/', [ProductController::class, 'product_list_page'])->name('product_list_page');
		});
		Route::group(['prefix' => 'transaction-status'], function () {
			Route::get('/', [TransactionStatusController::class, 'TransactionStatus_list_page'])->name('TransactionStatus_list_page');
		});
		Route::group(['prefix' => 'transaction-type'], function () {
			Route::get('/', [TransactionTypeController::class, 'TransactionType_list_page'])->name('TransactionType_list_page');
		});
	});
	
	Route::group(['prefix' => 'report'], function () {
		Route::group(['prefix' => 'stock-product'], function () {
			Route::get('/', [StockProductController::class, 'StockProduct_list_page'])->name('StockProduct_list_page');
		});
	});
	
	Route::group(['prefix' => 'transaction'], function () {
		Route::get('/struck', [SalesBillingController::class, 'struck_page'])->name('struck_page');
		Route::group(['prefix' => 'sales-billing'], function () {
			Route::get('/', [SalesBillingController::class, 'sales_billing_list_page'])->name('sales_billing_list_page');
			Route::post('/create', [SalesBillingController::class, 'save_sales_billing'])->name('save_sales_billing');
			Route::post('/update/{id}', [SalesBillingController::class, 'update_sales_billing'])->name('update_sales_billing');
		});
	});
	
	Route::group(['prefix' => 'mutation'], function () {
		Route::group(['prefix' => 'product'], function () {
			Route::get('/', [MutationProductController::class, 'mutation_product_list_page'])->name('mutation_product_list_page');
			Route::post('/create', [MutationProductController::class, 'save_mutation_product'])->name('save_mutation_product');
			Route::post('/update/{id}', [MutationProductController::class, 'update_mutation_product'])->name('update_mutation_product');
		});
	});
	
	Route::group(['prefix' => 'files','namespace'  => 'App\Http\Controllers'],
		function ($pathToFile) {
			Route::get('get-profile/{pathToFile}', 'UserController@getProfiles');
			Route::get('get-logo/{pathToFile}', 'UserController@getLogos');
			Route::get('get-billing/{pathToFile}', 'SalesBillingController@getBilling');
		}
	);
	
	Route::get('/get-menus', function(){
		//return Session::get('menu');
		Route::get('/', [Controller::class, 'get_menu_list'])->name('get_menu_list');
	});
	Route::get('/user-list', [UserController::class, 'user_list_data'])->name('user_list_data');
	Route::get('/role-list', [RoleController::class, 'role_list_data'])->name('role_list_data');
	Route::get('/menu-list', [MenuController::class, 'menu_list_data'])->name('menu_list_data');
	Route::get('/tenant-list', [TenantController::class, 'tenant_list_data'])->name('tenant_list_data');
	Route::get('/warehouse-list', [WarehouseController::class, 'warehouse_list_data'])->name('warehouse_list_data');
	Route::get('/uom-list', [UOMController::class, 'uom_list_data'])->name('uom_list_data');
	Route::get('/product-type-list', [ProductTypeController::class, 'product_type_list_data'])->name('product_type_list_data');
	Route::get('/product-list', [ProductController::class, 'product_list_data'])->name('product_list_data');
	Route::get('/product-dropdown-list', [ProductController::class, 'product_dropdown_list_data'])->name('product_dropdown_list_data');
	Route::get('/product-detail/{id}', [ProductController::class, 'product_detail'])->name('product_detail');
	Route::get('/transaction-status-list', [TransactionStatusController::class, 'TransactionStatus_list_data'])->name('TransactionStatus_list_data');
	Route::get('/transaction-type-list', [TransactionTypeController::class, 'TransactionType_list_data'])->name('TransactionType_list_data');
	Route::get('/stock-product-list', [StockProductController::class, 'StockProduct_list_data'])->name('StockProduct_list_data');
	Route::get('/sales-billing-list', [SalesBillingController::class, 'sales_billing_list_data'])->name('sales_billing_list_data');
	Route::get('/sales-billing/{id}', [SalesBillingController::class, 'sales_billing_detail'])->name('sales_billing_detail');
	Route::get('/mutation-product-list', [MutationProductController::class, 'mutation_product_list_data'])->name('mutation_product_list_data');
	Route::get('/mutation-product/{id}', [MutationProductController::class, 'mutation_product_detail'])->name('mutation_product_detail');
	Route::get('/transaction-type-dropdown-list', [TransactionTypeController::class, 'transaction_type_dropdown_list_data'])->name('transaction_type_dropdown_list_data');
});
