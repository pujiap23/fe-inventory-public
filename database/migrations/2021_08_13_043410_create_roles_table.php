<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('role_id');
			$table->string('role_type', 50);
			$table->string('role_name', 50);
			$table->string('description', 255);
			$table->boolean('is_active')->default(1);
			$table->boolean('is_delete')->default(0);
            $table->timestamps();
            $table->bigInteger('created_user')->unsigned();
            $table->foreign('created_user')->references('id')->on('users');
            $table->bigInteger('modified_user')->unsigned();
            $table->foreign('modified_user')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
