var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var monthFullNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function convertToDateTime(data){	
	var date = new Date(data);
    var strDate = date.getFullYear() + "-" + monthNames[date.getMonth()] + "-" + (date.getDate() > 10 ? date.getDate() : '0' + date.getDate());
    var strTime = (date.getHours() > 10 ? date.getHours() : '0' + date.getHours()) + ':' + (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes()) + ':' + (date.getSeconds() > 10 ? date.getSeconds() : '0' + date.getSeconds());
    return strDate  + " " + strTime; 
}

function convertToTime(data){
	var date = new Date(data);
    var strTime = (date.getHours() > 10 ? date.getHours() : '0' + date.getHours()) + ':' + (date.getMinutes() > 10 ? date.getMinutes() : '0' + date.getMinutes()) + ':' + (date.getSeconds() > 10 ? date.getSeconds() : '0' + date.getSeconds());
    return strTime; 
}

function convertToDate(data){
	var date = new Date(data);
    var strDate = date.getFullYear() + "-" + monthNames[date.getMonth()] + "-" + (date.getDate() > 10 ? date.getDate() : '0' + date.getDate());
    return strDate; 
}

function getFormatCurrentDate(dates){
	var date = new Date(dates);
	
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();
	var hour = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	if (day < 10) {
		day = '0' + day;
	}
	if ((month+1) < 10) {
		month = '0' + (month+1);
	}
	if(minutes<10){
		minutes = '0' + minutes;
	}
	if(seconds<10){
		seconds = '0' + seconds;
	}
	var formattedDate = day + '-' + month + '-' + year + ' ' + hour + ':' + minutes + ':' + seconds;
	return formattedDate;
}

function compareDate(dates){
	var date = new Date(dates);
	var currentDate = new Date();
	
	var year = date.getFullYear();
	var month = date.getMonth();
	var day = date.getDate();
	var yearCompare = currentDate.getFullYear();
	var monthCompare = currentDate.getMonth();
	var dayCompare = currentDate.getDate();
	if(year<yearCompare){
		formattedDate=false;
	}else if(month<monthCompare){
		formattedDate=false;
	}else if(day<dayCompare){
		formattedDate=false;
	}else{
		formattedDate=true;
	}
	return formattedDate;
}

