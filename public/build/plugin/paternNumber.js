function numeric(e){
	// Allow: backspace, delete,tab
	if ($.inArray(e.keyCode, [46, 8,9]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) ||
		 // Allow: Ctrl+C
		(e.keyCode == 67 && e.ctrlKey === true) ||
		// Allow: Ctrl+X
		(e.keyCode == 88 && e.ctrlKey === true) ||
		// Allow : Ctrl+V
		(e.keyCode == 86  && e.ctrlKey === true) ||
		 // Allow: home, end, left, right, down, up
		(e.keyCode >= 35 && e.keyCode <= 40)) {
		 // let it happen, don't do anything
		 return;
	}

	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

function specialCharacterCheck(e){
	if((e.keyCode > 96 && e.keyCode< 190) || (e.keyCode > 191 && e.keyCode< 223)){
		e.preventDefault();
	}
}

function specialCharacter(e){
	if($('#cmdSearchCategory').val()!="" || $('#cmdSearchCategory').val()!="undefined"){
		if($('#cmdSearchCategory').val()=="card" || $('#cmdSearchCategory').val()=="trace"){
			numeric(e);
		}else{
			specialCharacterCheck(e);
		}
	}
}

$(".specialCharacter").keydown(function(e){
	specialCharacter(e);
});

$('.number decimal').bind('cut copy paste', function (e) {
    e.preventDefault();
});

$(".number").keydown(function(e){
    numeric(e);
});

$(".decimal").keydown(function(e){
	decimal($(this).val(), e, $(this).attr('id'));
});

function change_to_currency(){
	var curr = document.getElementsByClassName("format-number").length;
	for(var a=0;a<curr;a++){
		document.getElementsByClassName("format-number")[a].innerHTML = document.getElementsByClassName("format-number")[a].innerHTML.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}

function decimal(objNum,e,id){
	if(objNum.indexOf('.') >= 0 && e.keyCode == 190)
	{
		e.preventDefault();
	}
	
	// Allow: backspace, delete,tab
	if ($.inArray(e.keyCode, [46, 8, 9, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) ||
		 // Allow: Ctrl+C
		(e.keyCode == 67 && e.ctrlKey === true) ||
		// Allow: Ctrl+X
		(e.keyCode == 88 && e.ctrlKey === true) ||
		 // Allow: home, end, left, right, down, up
		(e.keyCode >= 35 && e.keyCode <= 40)) {
		 // let it happen, don't do anything
		 return;
	}

	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

$('.notNullNumber').on('change', function(e){
	if($(this).val()=='.'){
		$(this).val('0');
	}
});