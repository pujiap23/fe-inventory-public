<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MutationProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->urlMutationProductList = $this->baseApiUrl . '/api/mutation-product/list';
        $this->urlMutationProductDetail = $this->baseApiUrl . '/api/mutation-product/detail/';
        $this->urlMutationProductCreate = $this->baseApiUrl . '/api/mutation-product/create';
        $this->urlMutationProductUpdate = $this->baseApiUrl . '/api/mutation-product/update/';
    }
	
    public function mutation_product_list_page(Request $request)
	{	
		$pageConfigs = ['pageHeader' => false];
		return view('/mutation/product', ['pageConfigs' => $pageConfigs]);
	}
	
	public function mutation_product_detail(Request $request, $id){
		$mutation_product = $this->httpClient->get($this->urlMutationProductDetail.$id, ['token'=>$request->session()->get('token')]);
		
		if ($mutation_product->successful()){
			//$dates = date('Y/m/d', strtotime($mutation_product->object()->created_at));
			$datas = $mutation_product->object();
			//$datas->mutation_date = $dates;
			return response()->json($datas);
		}else{
            return response()->json(['error' => $post->object()], $post->object());
		}
	}
	
	public function mutation_product_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'mutation_products.id', 1 => 'mutation_products.mutation_number', 2 => 'mutation_products.mutation_date', 3 => 'transaction_types.type_name', 4 => 'warehouse.warehouse_name', 5 => 'mutation_products.description');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlMutationProductList, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
	
	public function save_mutation_product(Request $request)
	{
        $validator = Validator::make(
            $request->all(),
            [
                'mutation_date'    			=> 'required|date',
                'transaction_type_id'    	=> 'required|integer',
                'description'   			=> 'nullable|string|between:0,255',
                'sub_mutation_product'     	=> 'required',
            ]
        );
		
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
		
		$mutation_product = array_merge(
                $validator->validated(),
                ['warehouse_id'=>$request->session()->get('warehouse')->id,'tenant_id'=>$request->session()->get('tenant')->id]
            );
		
		$post = $this->httpClient->withToken($request->session()->get('token'))->post($this->urlMutationProductCreate, $mutation_product);
		
		if ($post->successful()){
			//$this->save_txt_billing($request, $post->object()->sales_billing);
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()], $post->object()->status_code);
		}
	}
	
	public function update_mutation_product(Request $request, $id)
	{
        $validator = Validator::make(
            $request->all(),
            [
                'mutation_date'    			=> 'required|date',
                'transaction_type_id'    	=> 'required|integer',
                'description'   			=> 'nullable|string|between:0,255',
                'sub_mutation_product'     	=> 'required',
            ]
        );
		
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
		
		$mutation_product = array_merge(
                $validator->validated(),
                ['warehouse_id'=>$request->session()->get('warehouse')->id,'tenant_id'=>$request->session()->get('tenant')->id]
            );
		
		$post = $this->httpClient->withToken($request->session()->get('token'))->post($this->urlMutationProductUpdate.$id, $mutation_product);
		
		if ($post->successful()){
			//$this->save_txt_billing($request, $post->object()->sales_billing);
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()], $post->object()->status_code);
		}
	}
}
