<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlGetProductData = $this->baseApiUrl . '/api/product/list';
        $this->urlGetDropdownProductData = $this->baseApiUrl . '/api/product/drop-down-list';
        $this->urlGetProductDetailData = $this->baseApiUrl . '/api/product/detail/';
    }
	
    public function product_list_page (Request $request) {
		$pageConfigs = ['pageHeader' => false];
		return view('/product/product', ['pageConfigs' => $pageConfigs]);
	}
	
	public function product_dropdown_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$search = $request->input('search', '');
		
		$post = $this->httpClient->get($this->urlGetDropdownProductData, ['search'=>$search, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
	
	public function product_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'id', 1 => 'product_name', 2 => 'product_code', 3 => 'cost', 4 => 'selling_price', 5 => 'description');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlGetProductData, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
	
	public function product_detail(Request $request, $id)
	{
		$get = $this->httpClient->get($this->urlGetProductDetailData.$id, ['token'=>$request->session()->get('token')]);
		
		if ($get->successful()){
			return response()->json($get->object());
		}else{
			$message_error = array($get->object()->error);
            return response()->json(['error' => $get->object()->error], $get->object()->error->status_code);
		}
	}
}
