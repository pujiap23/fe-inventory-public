<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StockProductController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlGetStockProductData = $this->baseApiUrl . '/api/stock-product/list';
    }
	
    public function StockProduct_list_page (Request $request) {
		$pageConfigs = ['pageHeader' => false];
		return view('/stock-product/stock-product', ['pageConfigs' => $pageConfigs]);
	}
	
	public function StockProduct_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'id', 1 => 'warehouse_name', 2 => 'product_code', 3 => 'product_name', 4 => 'stock_awal', 5 => 'stock_keluar', 6 => 'stock_akhir');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlGetStockProductData, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
}
