<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionTypeController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlGetTransactionTypeData = $this->baseApiUrl . '/api/transaction-type/list';
        $this->urlGetDropdownTransactionTypeData = $this->baseApiUrl . '/api/transaction-type/drop-down-list';
    }
	
    public function TransactionType_list_page (Request $request) {
		$pageConfigs = ['pageHeader' => false];
		return view('/transaction-type/transaction-type', ['pageConfigs' => $pageConfigs]);
	}
	
	public function TransactionType_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'id', 1 => 'type_name', 2 => 'description');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlGetTransactionTypeData, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()], $post->object()->status_code);
		}
	}
	
	public function transaction_type_dropdown_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$search = $request->input('search', '');
		
		$post = $this->httpClient->get($this->urlGetDropdownTransactionTypeData, ['search'=>$search, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()], $post->object()->status_code);
		}
	}
}
