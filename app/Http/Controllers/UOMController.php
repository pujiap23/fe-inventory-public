<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UOMController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlGetUOMData = $this->baseApiUrl . '/api/uom/list';
    }
	
    public function uom_list_page (Request $request) {
		$pageConfigs = ['pageHeader' => false];
		return view('/uom/uom', ['pageConfigs' => $pageConfigs]);
	}
	
	public function uom_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'id', 1 => 'uom_name', 2 => 'description');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlGetUOMData, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
}
