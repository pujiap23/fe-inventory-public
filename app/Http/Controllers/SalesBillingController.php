<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Printing;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SalesBillingController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlSalesBillingList = $this->baseApiUrl . '/api/transaction-sales-billing/list';
        $this->urlSalesBillingDetail = $this->baseApiUrl . '/api/transaction-sales-billing/detail/';
        $this->urlSalesBillingCreate = $this->baseApiUrl . '/api/transaction-sales-billing/create';
        $this->urlSalesBillingUpdate = $this->baseApiUrl . '/api/transaction-sales-billing/update/';
    }
	
    public function sales_billing_list_page(Request $request)
	{	
		$pageConfigs = ['pageHeader' => false];
		return view('/sales_billing/sales_billing', ['pageConfigs' => $pageConfigs]);
	}
	
	public function sales_billing_detail(Request $request, $id){
		$billing_detail = $this->httpClient->get($this->urlSalesBillingDetail.$id, ['token'=>$request->session()->get('token')]);
		
		if ($billing_detail->successful()){
			$dates = date('d-m-Y H:i:s', strtotime($billing_detail->object()->created_at));
			$datas = $billing_detail->object();
			$datas->created_at = $dates;
			return response()->json($datas);
		}else{
            return response()->json(['error' => $post->object()->message], $post->object()->status_code);
		}
	}
	
    public function struck_page(Request $request)
	{	
		$id = $request->input('billing_id');
		$params = [];
		$billing_detail = $this->httpClient->get($this->urlSalesBillingDetail.$id, ['token'=>$request->session()->get('token')]);
		
		if ($billing_detail->successful()){
			$dates = date('d-m-Y H:i:s', strtotime($billing_detail->object()->created_at));
			//dd($billing_detail->object());
			//str_replace($billing_detail->object()->created_at,$dates,$billing_detail->object()->created_at);
			$datas = $billing_detail->object();
			$datas->created_at = $dates;
			
			$pageConfigs = ['pageHeader' => false];
			$params['pageConfigs'] = $pageConfigs;
			$params['billingDetails'] = $datas;
			
			return view('/sales_billing/struck', $params);
		}else{
            return response()->json(['error' => $post->object()->message], $post->object()->status_code);
		}
	}
	
	public function sales_billing_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'transaction_sales_billings.id', 1 => 'transaction_sales_billings.billing_number', 2 => 'transaction_sales_billings.customer_name', 3 => 'transaction_sales_billings.total_product', 4 => 'transaction_sales_billings.discount', 5 => 'transaction_sales_billings.total_discount', 6 => 'transaction_sales_billings.total_price', 7 => 'transaction_sales_billings.grant_total', 8 => 'transaction_sales_billings.total_payment', 9 => 'transaction_sales_billings.change_payment', 10 => 'transaction_statuses.status_name', 11 => 'warehouses.warehouse_name');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlSalesBillingList, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()->message], $post->object()->status_code);
		}
	}
	
	public function save_sales_billing(Request $request)
	{
        $validator = Validator::make(
            $request->all(),
            [
                'customer_id'    			=> 'nullable|integer',
                'customer_name'    			=> 'nullable|string|between:2,50',
                'transaction_status_id'     => 'required|integer',
                'total_product'   			=> 'integer',
                'discount'   				=> 'numeric',
                'total_discount'   			=> 'numeric',
                'total_price'   			=> 'numeric',
                'grant_total'   			=> 'numeric',
                'total_payment'   			=> 'numeric',
                'change_payment'   			=> 'numeric',
                'description'   			=> 'nullable|string|between:0,255',
                'sub_sales_billing'     	=> 'required'
            ]
        );
		
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
		$transaction_sales_billing = array_merge(
                $validator->validated(),
                ['warehouse_id'=>$request->session()->get('warehouse')->id,'tenant_id'=>$request->session()->get('tenant')->id]
            );
		
		$post = $this->httpClient->withToken($request->session()->get('token'))->post($this->urlSalesBillingCreate, $transaction_sales_billing);
		
		if ($post->successful()){
			//$this->save_txt_billing($request, $post->object()->sales_billing);
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()->message], $post->object()->status_code);
		}
	}
	
	public function update_sales_billing(Request $request, $id)
	{
        $validator = Validator::make(
            $request->all(),
            [
                'customer_id'    			=> 'nullable|integer',
                'customer_name'    			=> 'nullable|string|between:2,50',
                'transaction_status_id'     => 'required|integer',
                'total_product'   			=> 'integer',
                'discount'   				=> 'numeric',
                'total_discount'   			=> 'numeric',
                'total_price'   			=> 'numeric',
                'grant_total'   			=> 'numeric',
                'total_payment'   			=> 'numeric',
                'change_payment'   			=> 'numeric',
                'description'   			=> 'nullable|string|between:0,255',
                'sub_sales_billing'     	=> 'required'
            ]
        );
		
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
		$transaction_sales_billing = array_merge(
                $validator->validated(),
                ['warehouse_id'=>$request->session()->get('warehouse')->id,'tenant_id'=>$request->session()->get('tenant')->id]
            );
		
		$post = $this->httpClient->withToken($request->session()->get('token'))->post($this->urlSalesBillingUpdate.$id, $transaction_sales_billing);
		
		if ($post->successful()){
			//$this->save_txt_billing($request, $post->object()->sales_billing);
			return response()->json($post->object());
		}else{
            return response()->json(['error' => $post->object()->message], $post->object()->status_code);
		}
	}
	
	public function save_txt_billing(Request $request, $content){
		$tenant_name = $request->session()->get('tenant')->tenant_name;
		$tenant_address = $request->session()->get('tenant')->tenant_address;
		$tenant_address_1 = "";
		$tenant_address_2 = "";
		$tenant_address_space = "";
		$length_address = Str::length($request->session()->get('tenant')->tenant_address);
		if ($length_address > 39){
			$tenant_address_1 = substr($tenant_address, 0, 39);
			$tenant_address_2 = substr($tenant_address, 40, $length_address);
			$length_tenant_address_2 = Str::length(trim($tenant_address_2));
			$tenant_address_2 = $this->set_line_space($tenant_address_2);
		}
		$phone_number = $request->session()->get('tenant')->phone_number;
		/* Start Set Header */
		Storage::disk('local')->put('billing/'.$content->billing_number.'.txt', $this->set_line_space($tenant_name));
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', '---------------------------------------');
		if($tenant_address_1!="" and $tenant_address_2!=""){
			Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', $tenant_address_1);
			Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', $tenant_address_2);
		}else{
			Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', $this->set_line_space($tenant_address));
		}
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', $this->set_line_space($phone_number));
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', '---------------------------------------');
		/* End Set Header */
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', "Customer	: ".$content->customer_name);
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', "Cashier		: ".$request->session()->get('username'));
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', "Billing		: ".$content->billing_number);
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', "Date Time	: ".$content->created_at);
		Storage::disk('local')->append('billing/'.$content->billing_number.'.txt', '---------------------------------------');
		/*$myfile = fopen(storage_path('app/billing/').$content->billing_number.".txt", "w");
		fwrite($myfile, '            '.$tenant_name.'\n');
		fwrite($myfile, $tenant_address.'\n');
		fwrite($myfile, $phone_number.'\n');
		fwrite($myfile, '---------------------------------------\n');
		fclose($myfile);*/
	}
	
	public function set_line_space($char){
		$length_char = Str::length($char);
		$space = "";
		if ($length_char < 39){
				$lengths = 39-$length_char;
				$length_left_or_rigth = number_format($lengths/2);
				for ($a = 0;$a<$length_left_or_rigth;$a++){
					$space = $space." ";
				}
				$char = $space.$char.$space;
			}
		return $char;
	}
	
	public function getBilling($pathToFile)
    {
		if(file_exists(storage_path('app/billing/').$pathToFile)){
			return response()->file(storage_path('app/billing/').$pathToFile);
		}else{
			return response()->json(__('file_not_found'), 404);
		}
    }
}
