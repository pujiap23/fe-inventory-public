<?php

namespace App\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->urlLogin = $this->baseApiUrl . '/api/auth/login';
        $this->urlLogout = $this->baseApiUrl . '/api/auth/logout';
        $this->urlProfile = $this->baseApiUrl . '/api/user/profile';
        $this->urlRefresh = $this->baseApiUrl . '/api/user/refresh';
        $this->urlGetUserData = $this->baseApiUrl . '/api/user/list';
        $this->urlGetProfilePhotos = $this->baseApiFileUrl . '/get-profile/';
        $this->urlGetLogoPhotos = $this->baseApiFileUrl . '/get-logo/';
    }
	
	public function user_list_page(Request $request)
	{	
		$pageConfigs = ['pageHeader' => false];
		return view('/user/list', ['pageConfigs' => $pageConfigs]);
	}
	
	public function user_list_data(Request $request)
	{	
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$arr_order_column = array(0 => 'id', 1 => 'username', 2 => 'first_name', 3 => 'last_name', 4 => 'email');
		
		
		$start = $request->input('start', 0);
		$limit = $request->input('length', 10);
		$page = $request->input('draw', 1);
		$search = $request->input('search', '');
		
		// Get and mapping order column
		$order_column = $request->input('order_column', 0);
		$order = $arr_order_column[$order_column];
		
		// Get order dir
		$sort = $request->input('order_dir', 'asc');
		
		$post = $this->httpClient->get($this->urlGetUserData, ['page'=>$page, 'search'=>$search, 'start'=>$start, 'limit'=>$limit, 'order'=>$order, 'sort'=>$sort, 'token'=>$request->session()->get('token')]);
		
		if ($post->successful()){
			return response()->json($post->object());
		}else{
			$message_error = array($post->object()->error);
            return response()->json(['error' => $post->object()->error], $post->object()->error->status_code);
		}
	}
	
	public function getProfiles(Request $request)
    {
		$reqProfile = $this->httpClient->withToken($request->session()->get('token'))->get($this->urlGetProfilePhotos.$request->pathToFile);
		$filename = basename($reqProfile);
		$mimetype = MimeType::from($filename);
		$response = Response::make($reqProfile, 200);
        $response->header('Content-Type', 'image');
		return $response;
    }
	
	public function getLogos(Request $request)
    {
		$reqProfile = $this->httpClient->withToken($request->session()->get('token'))->get($this->urlGetLogoPhotos.$request->pathToFile);
		$filename = basename($reqProfile);
		$mimetype = MimeType::from($filename);
		$response = Response::make($reqProfile, 200);
        $response->header('Content-Type', 'image');
		return $response;
    }
	
	public function showLoginForm(Request $request)
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
		$request->session()->put('year_now', date("Y"));
		
		$post = $this->httpClient->post($this->urlRefresh, ['token'=>$request->session()->get('token')]);
		//$getUserProfile = $this->httpClient->withToken($request->session()->get('token'))->get($this->urlProfile);
		if ($post->successful()){
			$token = $post->object()->token;
			
			$reqProfile = $this->httpClient->withToken($token)->get($this->urlProfile);
            $request->session()->put('token', $token);
            $request->session()->put('authenticated', true);
            $request->session()->put('user', $post->object()->user);
            $request->session()->put('role', $post->object()->roles);
            $request->session()->put('warehouse', $post->object()->warehouse);
            $request->session()->put('menu', $post->object()->menus);
            $request->session()->put('email', $reqProfile->object()->email);
            $request->session()->put('username', $reqProfile->object()->username);
            $request->session()->put('full_name', $reqProfile->object()->full_name);
            $request->session()->put('url_profile', $reqProfile->object()->url_profile);
            $request->session()->put('tenant', $post->object()->tenant);
            $request->session()->put('tenant_id', $post->object()->tenant->id);
            $request->session()->put('tenant_logo', $post->object()->tenant->tenant_logo);
            $request->session()->put('tenant_name', $post->object()->tenant->tenant_name);
            $request->session()->put('tenant_address', $post->object()->tenant->tenant_address);
            $request->session()->put('tenant_number_phone', $post->object()->tenant->phone_number);
			$redirect = redirect()->route('home');
			#return $redirect;
			return redirect('home');
		}else{
			return view('/login/login', [
				'pageConfigs' => $pageConfigs
			]);
		}

        return view('/login/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'username'    => 'required',
                'password' => 'required|string|min:6',
            ]
        );
		
		$post = $this->httpClient->post($this->urlLogin, $request->all());
		
		if ($post->successful()){
			$token = $post->object()->token;
			
			$reqProfile = $this->httpClient->withToken($token)->get($this->urlProfile);
			echo print_r($post->object()->warehouse);
            $request->session()->put('token', $token);
            $request->session()->put('authenticated', true);
            $request->session()->put('user', $post->object()->user);
            $request->session()->put('role', $post->object()->roles);
            $request->session()->put('warehouse', $post->object()->warehouse);
            $request->session()->put('menu', $post->object()->menus);
            $request->session()->put('email', $reqProfile->object()->email);
            $request->session()->put('username', $reqProfile->object()->username);
            $request->session()->put('full_name', $reqProfile->object()->full_name);
            $request->session()->put('url_profile', $reqProfile->object()->url_profile);
            $request->session()->put('tenant', $post->object()->tenant);
            $request->session()->put('tenant_id', $post->object()->tenant->id);
            $request->session()->put('tenant_logo', $post->object()->tenant->tenant_logo);
            $request->session()->put('tenant_name', $post->object()->tenant->tenant_name);
            $request->session()->put('tenant_address', $post->object()->tenant->tenant_address);
            $request->session()->put('tenant_number_phone', $post->object()->tenant->phone_number);
			$redirect = redirect()->route('home');
			return redirect('home');
			//return $redirect;
		}else{
			$redirect = redirect()->back()->with('error', trans('messages.user_not_match'));
			return $redirect;
		}

    }//end login()
	
	public function logout(Request $request)
    {
		$pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
		
        $post = $this->httpClient->withToken($request->session()->get('token'))->post($this->urlLogout);
		
        if ($post->successful()) {

            $request->session()->invalidate();
            $request->session()->forget('token');
            $request->session()->forget('authenticated');
            $request->session()->forget('user');
            $request->session()->forget('role');
            $request->session()->forget('warehouse');
            $request->session()->forget('menu');
            $request->session()->forget('user');
            $request->session()->forget('email');
            $request->session()->forget('username');
            $request->session()->forget('full_name');
            $request->session()->forget('url_profile');
            $request->session()->forget('tenant');
            $request->session()->forget('tenant_id');
            $request->session()->forget('tenant_logo');
            $request->session()->forget('tenant_name');
            $request->session()->forget('tenant_address');
            $request->session()->forget('tenant_number_phone');
            $redirect = redirect()->route('auth-login');
        } else {
			$redirect = redirect()->back()->with('error', "Logout");
			
            $request->session()->forget('token');
            $request->session()->forget('authenticated');
            $request->session()->forget('user');
            $request->session()->forget('role');
            $request->session()->forget('warehouse');
            $request->session()->forget('menu');
            $request->session()->forget('user');
            $request->session()->forget('email');
            $request->session()->forget('username');
            $request->session()->forget('full_name');
            $request->session()->forget('url_profile');
            $request->session()->forget('tenant');
            $request->session()->forget('tenant_id');
            $request->session()->forget('tenant_logo');
            $request->session()->forget('tenant_name');
            $request->session()->forget('tenant_address');
            $request->session()->forget('tenant_number_phone');
            $redirect = redirect()->route('auth-login');
        }
        return $redirect;
    }//end logput()
}
