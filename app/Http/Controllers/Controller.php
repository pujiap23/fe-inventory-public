<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
    protected $baseApiUrl;
    protected $baseApiFileUrl;
    protected $mainBaseUrl;
    protected $fileBaseUrl;
    protected $httpClient;
	
    public function __construct()
    {
        $this->baseApiUrl = config('app.api_url');
        $this->baseApiFileUrl = config('app.api_file_url');
        $this->httpClient = Http::accept('application/json');
    }
	
    public function get_menu_list(Request $request)
	{	
		$params = [];
		$pageConfigs = ['pageHeader' => false];
		
		$params['pageConfigs'] = $pageConfigs;
		$params['menus'] = $request->session()->get('menu');
		dd($params);
		return $params;
	}
}
