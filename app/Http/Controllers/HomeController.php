<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
	public function home(Request $request)
	{	
		$pageConfigs = ['pageHeader' => false];

		return view('/home/home', ['pageConfigs' => $pageConfigs]);
	}
}
