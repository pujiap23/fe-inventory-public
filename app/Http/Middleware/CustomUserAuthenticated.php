<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CustomUserAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		
		if ($request->lang != ""){
			 App::setLocale($request->lang);
		}else{
			 App::setLocale(App::getLocale());
		}
		
        if ($request->session()->exists('token') && $request->session()->exists('authenticated') && $request->session()->get('authenticated', false)) {
            // user value cannot be found in session
            return $next($request);
        }

        return redirect()->route('auth-login');
    }
}
