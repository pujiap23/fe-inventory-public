<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	   	<title>Inventory - Login</title>
		<link rel="icon" href="../images/favicon.ico">

	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">	
	    <link href="../vendors/custom/css/custom.min.css" rel="stylesheet">
		<link href="../vendors/pnotify/pnotify.custom.css" rel="stylesheet">

	</head>

  	<body class="login">
    	<div class="login_wrapper">
        	<section class="login_content">
            	<form class="form-horizontal" action="{{ route('auth-login-process') }}" method="POST">
					{{ csrf_field() }}
              		<h1>Login Form</h1>
					<div class="demo-spacing-0">
						@if ($message = Session::get('error'))
						<div class="alert alert-danger" role="alert">
							<h4 class="alert-heading">Oopss! {{$message}}</h4>
						</div>
						@endif
					</div>
              		<div class="form-group">
						<input class="form-control" id="login-email" type="text" name="username" value="{{ old('username') }}" placeholder="{{__('username')}}" aria-describedby="login-email" autofocus="" tabindex="1" />
              		</div>
              		<div class="form-group">
                        <input class="form-control form-control-merge" id="login-password" type="password" name="password" placeholder="{{__('password')}}" aria-describedby="login-password" tabindex="2" />
              		</div>
              		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
              		<div class="form-group">
                		<button class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                	</div>
              		<div class="clearfix"></div>
              		<div class="separator">
	                	<div class="clearfix"></div>
	                	<br/>
	                	<div>
	                  		<div class="row">
								<div class="col-xs-12">
								</div>
							</div>
							<h1></h1>
	                  		<p>Copyright <i class="fa fa-copyright" aria-hidden="true"></i> {{Session::get('year_now')}} ZYP. Privacy and Terms</p>
	                	</div>
					</div>
            	</form>
                <!--<form class="auth-login-form mt-2" action="{{ route('auth-login-process') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="form-label" for="login-username">Username</label>
                        <input class="form-control" id="login-username" type="text" name="username" value="{{ old('username') }}" placeholder="username" aria-describedby="login-username" autofocus="" tabindex="1" />
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <label for="login-password">Password</label>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input class="form-control form-control-merge" id="login-password" type="password" name="password" placeholder="password" aria-describedby="login-password" tabindex="2" />
                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                        </div>
                    </div>
                    <br />
                    <button class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                </form>-->
			</section>
		</div>
		
		<script src="../vendors/jquery/dist/jquery.min.js"></script>
		<script src="../vendors/pnotify/pnotify.custom.js"></script>
		<script>
			var stack_bottomright = {"dir1": "right", "dir2": "up", "push": "top"};
			$("#txtUsername").focus();
			$("#btnLoginForm").on("click",function(){
				authValidations();
			});
			
			function authValidations(){
				var parameter={
						"username":$("#txtUsername").val(),
		        		"password":$("#txtPassword").val()
				};

				$.ajax({
		            type: "POST",
		            contentType: "application/json",
		            url: "../api/login",
		           	data:JSON.stringify(parameter),
					headers:{"Accept":"application/json"},
		            dataType: 'json',
		            cache: false,
		            timeout: 5000,
		            success: function (data) {
		            	window.location.href=data;
		            },
		            error: function (e) {
		            	if(e.status==0 || e.status==403 || e.status==500){
			    			showModal("", "Koneksi terputus")
		            	}
		            }
				});
			}

			function showNotif(type, message) {
				var typeStr = '';
				var title = '';
				if(type == '200' || type == '201'){
					typeStr = 'success';
					title = '<spring:message code="lbl.info"/>';
				}else if(type == '400' || type == '404'){
					typeStr = 'info';
					title = '<spring:message code="lbl.info"/>';
				}else{
					typeStr = 'error';
					title = '<spring:message code="lbl.error"/>';
				}
				var opts = {
					type: typeStr,
					title: title,
					text: message,
					delay: 2000,
					addclass: "stack-bottomright",
					stack: stack_bottomright
				};
				new PNotify(opts);
			}
		</script>
	</body>
</html>
