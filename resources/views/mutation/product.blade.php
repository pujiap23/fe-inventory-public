@include('template/head-open')
	<title>Inventory - Mutation Product List</title>
	<link rel="icon" href="../images/favicon.ico">
	
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
@include('template/head-close')
@include('template/body-open')
@include('template/div-open')
	<!-- page content -->
	<div class="right_col" role="main">
		<div id="div-element-list">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('lbl_mutation_product')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="height: auto;display: none">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input type='text' id="txtSearch" placeHolder="{{ __('lbl_search')}}" class="form-control" />
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnShow" class="btn btn-primary form-control">{{ __('lbl_show')}}</button>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnAdd" class="btn btn-success form-control"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;{{ __('lbl_add')}}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<table id="datatable" class="table table-striped table-bordered" style="cursor: pointer;">
								<thead>
									<tr>
                          				<th class="sorting_table" value="0" hidden="true">{{ __('lbl_mutation_id')}}</th>
                          				<th class="sorting_table" value="1">{{ __('lbl_mutation_number')}}</th>
                          				<th class="sorting_table" value="2">{{ __('lbl_mutation_date')}}</th>
                          				<th class="sorting_table" value="3">{{ __('lbl_transaction_type')}}</th>
                          				<th class="sorting_table" value="4">{{ __('lbl_warehouse_name')}}</th>
                          				<th class="sorting_table" value="5">{{ __('lbl_description')}}</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="" id="div-element-add" hidden="true">
			<div class="row" id="div-element-content">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('lbl_mutation_product')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<form class="form-horizontal">
								<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class='input-group date' id='myDatepicker1' style="margin-bottom:0px !important">
											<input id="txtMutationDate"  type='text' class="form-control" placeHolder="{{__('lbl_mutation_date')}}" />
											<span class="input-group-addon">
											   <span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type='text' id="txtCreatedBy" class="form-control" value="{{Session::get('username')}}" readonly />
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control" id="cmbMutationType">
											<option value="">{{__('lbl_choose')}}</option>
										</select>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type='text' style="height: 38px;" id="txtDescription" class="form-control" placeHolder="{{__('lbl_description')}}"/>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-8 col-sm-8 col-xs-12">
										<select class="form-control" id="cmbProduct">
											<option value="">{{__('lbl_choose')}}</option>
										</select>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-12">
										<input type='text' style="height: 38px;" id="txtQuantity" class="form-control number" value="1" placeHolder="{{__('lbl_quantity')}}"/>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" style="height: 38px;" id="buttonAddProduct" class="btn btn-success form-control" data-dismiss="modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{__('lbl_add')}}</button>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive">
											<table id="datatable-product" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="text-center" width="10%" value="1">{{ __('lbl_product_name')}}</th>
														<th class="text-center" width="10%" value="2">{{ __('lbl_product_code')}}</th>
														<th class="text-right" width="10%" value="3">{{ __('lbl_quantity')}}</th>
														<th class="text-center" width="10%" value="4">{{ __('lbl_uom')}}</th>
														<th class="text-center" width="10%" value="5">{{ __('lbl_delete')}}</th>
													</tr>
												</thead>
											<tbody id="body-table-product"></tbody>
											</table>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<button type="button" style="height: 38px;" id="buttonCancel" class="btn btn-danger form-control"><i class="fa fa-close"></i>&nbsp;&nbsp;{{__('lbl_cancel')}}</button>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<button type="button" style="height: 38px;" id="buttonSubmit" class="btn btn-success form-control"><i class="fa fa-save"></i>&nbsp;&nbsp;{{__('lbl_save')}}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- page content -->
@include('template/div-close')
@include('template/core-js')
<!-- Datatables -->
	<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="../build/js/jquery.spring-friendly.js"></script>
	<script>
		var status_transaction = "add";
		var sub_product = [];
		var number = 0;
		var this_id = null;
		$("#cmbMutationType").select2();
		$("#cmbProduct").select2();
		getAllProduct();
		getAllTransactionType();
		$(document).ready(function() {
			datatable();
		});
		
		$('#btnAdd').click(function () {
			status_transaction = "add";
			$('#div-element-list').attr('hidden',true);
			$('#div-element-add').attr('hidden',false);
		});
		
		$('#btnShow').click(function () {
			datatable();
		});
		
		$('#buttonAddProduct').click(function () {
			addProduct();
		});
		
		$('#buttonCancel').click(function () {
			clear_form();
			$('#div-element-list').attr('hidden',false);
			$('#div-element-add').attr('hidden',true);
		});
		
		$('#buttonSubmit').click(function () {
			if(sub_product.length > 0){
				if (status_transaction=="add"){
					saveTransaction();
				}else if (status_transaction=="edit"){
					status_transaction="update";
					//get_detail(this_id);
					$("#div-element-content *").attr("disabled", false);
					$("#div-element-content *").attr("readonly", false);
					$("#cmbMutationType").attr("disabled", false);
					$("#cmbProduct").attr("disabled", false);
					document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-save'></i>&nbsp;&nbsp;{{__('lbl_update')}}";
				}else{
					updateTransaction();
				}
			}else{
				showNotif('', 'Product list is empty');
			}
		});
		
		$("#txtQuantity").on("keyup", function(e){
			if(e.keyCode==13){
				addProduct();
			}
		});
		
		$('#myDatepicker1').datetimepicker({
			showClear: true,
			showTodayButton: true,
			useCurrent: false,
			format: 'YYYY-MM-DD'
		});
		
		var a = new Date();
		a.setDate(a.getDate()+1);
		$('#myDatepicker1').data("DateTimePicker").maxDate(a);
		
		function clear_form(){
			for (var a=0;a<sub_product.length;a++){
				var trelement = document.getElementById("tr-"+sub_product[a].initial);
				if(trelement){
					trelement.remove();
				}
			}
			sub_product = [];
			$("#txtDescription").val("");
			$("#txtCreatedBy").val("{{Session::get('username')}}");
			$("#cmbProduct").val("");
			$("#cmbProduct").trigger('change');
			$("#cmbMutationType").val("");
			$("#cmbMutationType").trigger('change');
			$("#txtMutationDate").val("");
			$("#div-element-content *").attr("disabled", false);
			document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-save'></i>&nbsp;&nbsp;{{__('lbl_save')}}";
			number = 0;
		}
		
		function deleteData(id) {
			if (status_transaction == "add"){
				var myobj = document.getElementById("tr-"+id);
				myobj.remove();
				const dataRemoved = sub_product.filter((el) => {
					return el.initial !== id;
				});
				sub_product = dataRemoved;
			}else{
				var myobj = document.getElementById("tr-"+id);
				document.getElementById("quantity-"+id).innerHTML = 0;
				const dataRemoved = sub_product.filter((el) => {
					if (el.initial == id){
						el.quantity = 0;
					}
					return el;
				});
				sub_product = dataRemoved;
			}
		}
		
		function addProduct(){
			var qty = Number($("#txtQuantity").val());
			var product = $("#cmbProduct").val();
			if(Number(qty) > 0  && product!=""){
				getAndSetProduct();
			}else{
				if (Number(qty) == 0){
					$("#txtQuantity").focus();
					showNotif('400', '{{__("msg_required_quantity")}}');
				}else{
					$("#cmbProduct").focus();
					showNotif('400', '{{__("msg_required_selected_product")}}');
				}
			}
		}
		
		function get_detail(id){
			$('#div-element-list').attr('hidden',true);
			$('#div-element-add').attr('hidden',false);
	
			$("#div-element-content *").attr("readonly", "readonly");
			$("#cmbMutationType").attr("disabled", "disabled");
			$("#cmbProduct").attr("disabled", "disabled");
			document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-pencil'></i>&nbsp;&nbsp;{{__('lbl_edit')}}";
			
			$.ajax({
				type : "GET",
				url : "../mutation-product/" + id,
				timeout: 100000,
				success : function(data) {
					console.log(data);
					document.getElementById("body-table-product").innerHTML = "";
					sub_product = [];
					number = 0;
					this_id = id;
					var str = "";
					for(var a=0;a<data.mutation_sub_product.length;a++){
						number = number + 1;
						qty = data.mutation_sub_product[a].quantity;
						total = (Number(data.mutation_sub_product[a].product_id.selling_price)) * (Number(qty));
						str += '<tr id="tr-'+number+'">';
						str += '<td class="text-center">'+data.mutation_sub_product[a].product_id.product_name+'</td>';
						str += '<td class="text-center">'+data.mutation_sub_product[a].product_id.product_code+'</td>';
						str += '<td class="text-right" id="quantity-'+number+'">'+qty+'</td>';
						str += '<td class="text-center">'+data.mutation_sub_product[a].product_id.uom_id.uom_name+'</td>';
						str += '<td class="text-center"><button type="button" disabled onclick="deleteData('+number+')" title="{{ __("lbl_delete")}}" data-id="'+ number +'" class="btn btn-xs btn-delete delete-product" style="margin-bottom:0px;background-color:white;color: black;border: 2px solid #e7e7e7;" data-toggle="modal" data-target="#myModal"><a><i class="fa fa-fw fa-trash" style="color: red;"></i></a></button></td>';
						str += '</tr>';
						
						var arr_product = {
											"initial":number,
											"id":data.mutation_sub_product[a].id,
											"product_id":data.mutation_sub_product[a].product_id.id,
											"quantity":Number(qty)
										};
						sub_product.push(arr_product);
					}

					//document.getElementById("body-table-product").innerHTML = str;
					$("#body-table-product").append(str);
					$("#txtCreatedBy").val(data.created_by.username);
					$("#txtMutationDate").val(data.mutation_date);
					$("#txtDescription").val(data.description);
					$("#cmbMutationType").val(data.transaction_type_id.id);
					$("#cmbMutationType").trigger('change');
					
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getAllTransactionType(){
			$.ajax({
				type : "GET",
				url : "../transaction-type-dropdown-list",
				timeout: 100000,
				success : function(data) {
					console.log(data);
					for(var a=0;a<data.length;a++){
						$("#cmbMutationType").append("<option value='"+data[a].id+"'>"+ data[a].type_name+"</option>")
					}
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getAndSetProduct(){
			$.ajax({
				type : "GET",
				url : "../product-detail/"+$("#cmbProduct").val(),
				timeout: 4000,
				success : function(data) {
					var str = "";
					number = number + 1;
					qty = $("#txtQuantity").val();
					total = (Number(data.selling_price)) * (Number(qty));
					str += '<tr id="tr-'+number+'">';
					str += '<td class="text-center">'+data.product_name+'</td>';
					str += '<td class="text-center">'+data.product_code+'</td>';
					str += '<td class="text-right">'+$("#txtQuantity").val()+'</td>';
					str += '<td class="text-center">'+data.uom_id.uom_name+'</td>';
					str += '<td class="text-center"><button type="button" onclick="deleteData('+number+')" title="{{ __("lbl_delete")}}" data-id="'+ number +'" class="btn btn-xs btn-delete delete-product" style="margin-bottom:0px;background-color:white;color: black;border: 2px solid #e7e7e7;" data-toggle="modal" data-target="#myModal"><a><i class="fa fa-fw fa-trash" style="color: red;"></i></a></button></td>';
					str += '</tr>';
					
					$("#body-table-product").append(str);
					
					var arr_product = {
										"id":null,
										"initial":number,
										"product_id":data.id,
                                        "quantity":Number(qty)
									};
					sub_product.push(arr_product);
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getAllProduct(){
			$.ajax({
				type : "GET",
				url : "../product-dropdown-list",
				timeout: 100000,
				data: {
						search:""					
				},
				success : function(data) {
					for(var a=0;a<data.length;a++){
						$("#cmbProduct").append("<option value='"+data[a].id+"'>"+data[a].product_code + " - " + data[a].product_name+"</option>")
					}
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function saveTransaction(){
			data = {
				"_token": "{{ csrf_token() }}",
                'transaction_type_id'    	: $('#cmbMutationType').val(),
                'mutation_date'    			: $('#txtMutationDate').val(),
                'description'    			: $('#txtDescription').val(),
                'sub_mutation_product'     	: sub_product
			}
			
			$.ajax({
				type : "POST",
				url : "../mutation/product/create",
				timeout: 10000,
				accept:'application/json',
				data:JSON.stringify(data),
				contentType:'application/json',
				success : function(data) {
					clear_form();
					$('#div-element-list').attr('hidden',false);
					$('#div-element-add').attr('hidden',true);
					showNotif('200', 'Successfuly created mutation' + " " + data.mutation_product.mutation_number);
					datatable();
				},
				error : function(textStatus) {
					//errAjaxHandler(textStatus);
					showNotif(textStatus.status, textStatus.responseText);
				},
			});
		}
		
		function updateTransaction(){
			data = {
				"_token": "{{ csrf_token() }}",
                'transaction_type_id'    	: $('#cmbMutationType').val(),
                'mutation_date'    			: $('#txtMutationDate').val(),
                'description'    			: $('#txtDescription').val(),
                'sub_mutation_product'     	: sub_product
			}
			
			$.ajax({
				type : "POST",
				url : "../mutation/product/update/"+this_id,
				timeout: 10000,
				accept:'application/json',
				data:JSON.stringify(data),
				contentType:'application/json',
				success : function(data) {
					clear_form();
					$('#div-element-list').attr('hidden',false);
					$('#div-element-add').attr('hidden',true);
					//$('#div-element-billing').attr('hidden',false);
					showNotif('200', 'Successfuly update mutation' + " " + data.mutation_product.mutation_number);
					datatable();
				},
				error : function(textStatus) {
					//errAjaxHandler(textStatus);
					showNotif(textStatus.status, textStatus.responseText);
				},
			});
		}
		
		function datatable(){
			var table = $('#datatable').DataTable({
				'ajax':
				{
					url:'../mutation-product-list',
					data: {
						search:$('#txtSearch').val()
					},
					error: function (e) {
						NProgress.done();
						showNotif(e.status, e.responseJSON.error.message);
					}
				},
				'paging'      : true,
			    'lengthChange': false,
			    'destroy'	  : true,
			    'searching'   : false,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : false,
			    'processing'  : true,
			    'serverSide'  : true,
				'order' :[[ 0, "desc" ]],
				'columns'     : [
				{
					data : 'id',
					'visible' : false
				}, {
					data : 'mutation_number'
				}, {
					data : 'mutation_date'
				}, {
					data : 'type_name'
				}, {
					data : 'warehouse_name'
				}, {
					data : 'description'
				}]
			}).on('click', 'tbody tr', function () {
				var data = table.row(this).data();
				status_transaction = "edit";
				get_detail(data.id);
			});
		}
	</script>
@include('template/body-close')