@include('template/head-open')
	<title>Inventory - Home</title>
	<link rel="icon" href="../images/favicon.ico">
	
@include('template/head-close')
@include('template/body-open')
@include('template/div-open')
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<img src="../files/get-logo/{{Session::get('tenant_logo')}}" class="img img-responsive" style="height:200px;margin:0 auto; padding-top: 20px; padding-bottom: 30px">
					<!--<img src="../images/logo_company.png" class="img img-responsive" style="margin:0 auto; padding-top: 20px; padding-bottom: 30px"/>-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="../images/line.png" class="img img-responsive" style="padding-bottom:20px"/>
					<div class="text-center" style="font-size:38px;color:#4491cc;font-weight:bold;outline-color:white;text-shadow:-2px -2px 0 #fff,  2px -2px 0 #fff,-2px 2px 0 #fff,2px 2px 0 #fff;; ">{{Session::get('tenant_name')}}</div>
					<div class="text-center" style="font-size:15px;color:black;font-weight:bold;outline-color:white;text-shadow:-2px -2px 0 #fff,  2px -2px 0 #fff,-2px 2px 0 #fff,2px 2px 0 #fff;; ">{{Session::get('tenant_address')}}</div>
					<div class="text-center" style="font-size:15px;color:black;font-weight:bold;outline-color:white;text-shadow:-2px -2px 0 #fff,  2px -2px 0 #fff,-2px 2px 0 #fff,2px 2px 0 #fff;; ">{{Session::get('tenant_number_phone')}}</div>
					<!-- <div class="text-center" style="font-size:18px;color:#4491cc; ">v. 0.0.0.1 ${appVersion.appVersion}</div>
					<div class="text-center" style="font-size:18px;color:#4491cc;">Last Update: <fmt:formatDate value="${appVersion.changeDate}" pattern="dd-MMM-yyyy" /></div>-->
					<img src="../images/line.png" class="img img-responsive" style="padding-top:20px;padding-bottom:20px"/>
				</div>
			</div>
			<!--<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					Change Log:
					<div class="x_panel">
						<div class="x_content">
							<div class="col-md-12" style="height: 153px; overflow-y: scroll;">
													
							</div>
							
						</div>
					</div>
				</div>
			</div>-->
		</div>
	</div>
	<!-- page content -->
    
@include('template/div-close')

@include('template/core-js')
@include('template/body-close')