<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">	
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    
    <!-- Flag -->
    <link href="../vendors/flag-icon/css/flag-icon.css" rel="stylesheet">
	<link href="../vendors/flag-icon/css/flag-icon.min.css" rel="stylesheet">
	
	<!-- PNotify -->
	<link href="../vendors/pnotify/pnotify.custom.css" rel="stylesheet">

	<!-- Select2 -->
	<link href="../vendors/select2-4.0.6-rc.1/dist/css/select2.css" rel="stylesheet">
	
	<!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
	<style>
		.select2 {
			width:100%!important;
		}
		
		.bd-example-modal-lg .modal-dialog{
		    display: inherit;
		    position: relative;
		    margin: 0 auto;
		    top: calc(50% - 24px);
		    text-align: center;
	  	}
	</style>