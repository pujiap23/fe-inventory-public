<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Custom Theme Scripts -->
<script src="../build/js/custom.js"></script>
<!-- PNotify -->
<script src="../vendors/pnotify/pnotify.custom.js"></script>
<!-- Select2 -->
<script src="../vendors/select2-4.0.6-rc.1/dist/js/select2.js"></script>
<!-- Select2 -->
<script src="../build/plugin/paternNumber.js"></script>
<script src="../build/plugin/renderDateTime.js"></script>
<script src="../build/plugin/escapeHTML.js"></script>

<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->    
<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script>
	
	var stack_bottomright = {"dir1": "right", "dir2": "up", "push": "top"};

    //setTimeout(function() {getAccess();}, 0);
	getAccess();
	
	function getAccess(){
		$.ajax({
	        type: "GET",
	        contentType : 'application/json',
	        url: "../get-menus",
	        beforeSend: function(){
	        	NProgress.start();
	        },
	        success: function (data) {
	        	/*for(a=0;a<data.length;a++){
					if (data[a].sub_menu != undefined){
						if(data[a].parent_id==null){
							$('#ul-main-menu').append("<li id='menu-"+data[a].menu_id+"'><a><i class='"+data[a].menu_icon+"'></i>"+data[a].menu_name+"<span class='fa fa-chevron-down'></span></a></li>");
						}
						for (b=0;b<data[a].sub_menu.length;b++){
							
							var myEle = document.getElementById("ul-menu-"+data[a].sub_menu[b].parent_id);
							
							if(myEle){
								$('#ul-menu-'+data[a].sub_menu[b].parent_id).append("<li id='li-sub-menu-"+data[a].sub_menu[b].menu_id+"'><a href='"+data[a].sub_menu[b].menu_path+"'><i class='"+data[a].sub_menu[b].menu_icon+"'></i><span class='text'>"+data[a].sub_menu[b].menu_name+"</span></a></li>");
							}else{
								$('#menu-'+data[a].sub_menu[b].parent_id).append("<ul class='nav child_menu' id='ul-menu-"+data[a].sub_menu[b].parent_id+"'></ul>");
								$('#ul-menu-'+data[a].sub_menu[b].parent_id).append("<li id='li-sub-menu-"+data[a].sub_menu[b].menu_id+"'><a href='"+data[a].sub_menu[b].menu_path+"'><i class='"+data[a].sub_menu[b].menu_icon+"'></i><span class='text'>"+data[a].sub_menu[b].menu_name+"</span></a></li>");
							}
						}
					}else{
						$('#ul-main-menu').append("<li id='menu-"+data[a].menu_id+"'><a href='"+data[a].menu_path+"'><i class='"+data[a].menu_icon+"'></i>"+data[a].menu_name+"</a></li>");
					}
	        	}*/
	        	NProgress.done();
	        },
	        error: function (e) {
	        	NProgress.done();
	        	if(e.status == '403' || e.status == '404'){
	            	showNotif(e.status, getMessage(e));
	        	}
	        	else
	        	{
	            	showNotif(e.status, getMessage(e));		            		
	        	}
	        }
	    });
	}
	
	function showNotif(type, message) {
		var typeStr = '';
		var title = '';
		if(type == '200' || type == '201'){
			typeStr = 'success';
			title = 'Info';
		}else if(type == '400' || type == '404'){
			typeStr = 'info';
			title = 'Info';
		}else{
			typeStr = 'error';
			title = 'Error';
		}
		var opts = {
			type: typeStr,
			title: title,
			text: message,
			delay: 2000,
			addclass: "stack-bottomright",
			stack: stack_bottomright
		};
		new PNotify(opts);
	}
	
	function showNotifOn(type, message) {
		var typeStr = '';
		var title = '';
		if(type == '200' || type == '201'){
			typeStr = 'success';
			title = 'Info';
		}else if(type == '400' || type == '404'){
			typeStr = 'info';
			title = 'Info';
		}else{
			typeStr = 'error';
			title = 'Error';
		}
		var opts = {
			type: typeStr,
			title: title,
			text: message,
			addclass: "stack-bottomright",
			stack: stack_bottomright
		};
		new PNotify(opts);
	}
	
	$(document).ready(function() {
		$('.ui-pnotify').remove();
		setFlag();
		
		$('#changePass').click(function (){
			$('#txtUsername').val("{{Session::get('user')->username}}");
			$('#txtFirstName').val("{{Session::get('user')->first_name}}");
			$('#txtLastName').val("{{Session::get('user')->last_name}}");
			$('#txEmail').val("{{Session::get('user')->email}}");
			$('#txtOldPassword').val('');
			$('#txtNewPassword').val('');
			$('#txtConfirmPassword').val('');
			$('.bs-example-modal-pass').modal('show');
		});
		
		$('#btnClosePass').click(function () {
			if($('#txtOldPassword').val()==""){
				$('#modalChangePassword').modal('hide');
			}else{
				new PNotify({
					title: '{{__("lbl_confirm")}}',
					text: '{{__("lbl_password_cancel")}}',
					icon: 'glyphicon glyphicon-question-sign',
					hide: false,
					confirm: {
				  		confirm: true,
				  		buttons: [{
				  			text: '{{__("lbl_ok")}}'
				  		},{
				  			text: '{{__("lbl_cancel")}}'
				  		}]
					},
					buttons: {
				  		closer: false,
				  		sticker: false
					},
					history: {
				  		history: false
					},
					addclass: 'stack-modal',
					stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
				}).get().on('pnotify.confirm', function(){
					$('#modalChangePassword').modal('hide');
				});
			}
		});
		
		$('#btnSavePass').click(function () {
			var formData = new FormData();
			formData.append("tenantName", "asdasda");
			console.log(formData);
			
			var data = new FormData();
			data.append("username", $('#txtUsername').val());
			data.append("first_name", $('#txtFirstName').val());
			data.append("last_name", $('#txtLastName').val());
			data.append("email", $('#txEmail').val());
			data.append("url_profile", $('#uploadProfile').val());
			data.append("warehouse_id", "{{Session::get('warehouse')->id}}");
			data.append("tenant_id", "{{ Session::get('tenant')->id}}");
			data.append("role_id", "{{ Session::get('role')->id}}");
			data.append("new_pass", $('#txtNewPassword').val());
			data.append("confirm_pass", $('#txtConfirmPassword').val());
			if($('input[type=file]')[0].files[0]){
				data.append("url_profile", $('input[type=file]')[0].files[0]);
			}
			
			console.log(data);
			
			$.ajax({
	            type: "POST",
	            contentType : 'application/json; charset=utf-8',
	            url: "../update-profile/{{ Session::get('user')->user_id}}",
	            data: data,
	            beforeSend: function(){
	            	NProgress.start();
	            },
	            success: function (data) {
	            	NProgress.done();
	            	showNotif(data.status, data.message);
	            	if(data.status == '201' || data.status == '200'){
		            	setTimeout(function() {
		            		$('.bs-example-modal-pass').modal('hide');
		            	}, 1000);		            		
	            	}
	            },
	            error: function (e) {
	            	NProgress.done();
	            	if(e.status == '403' || e.status == '404'){
		            	showNotif(e.status, getMessage(e));
	            	}
	            	else
	            	{
		            	showNotif(e.status, getMessage(e));		            		
	            	}
	            }
	        }); 
		});
	});
	
	function changeLanguage(language) {
		var href = window.location.toString();
		href = href.replace('#', '');
		if(href.indexOf("?") >= 0){
			href = href.substr(0, href.indexOf('?'));
		}
		window.location.replace(href + '?lang=' + language);
	}
	
	function setFlag() {
		var flagCurrLanguage = document.getElementById("currentLanguage");
	    var currLanguage = "{{ app()->getLocale() }}";
	    currLanguage = currLanguage == 'in' ? 'id' : currLanguage;
	    currLanguage = currLanguage == 'en' ? 'gb' : currLanguage;
	    currLanguage = currLanguage == '' ? 'en' : currLanguage;
	    
	    flagCurrLanguage.classList.add(('flag-icon-' + currLanguage));
	}
	
	function getCookie(cname) {
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i <ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}
	
	function getMessage(e){
		try{
			if(e.responseText.indexOf('<!--error') >= 0 && e.responseText.indexOf('error-->') >= 0){
				return 	e.responseText.substring(e.responseText.indexOf('<!--error') + 9,e.responseText.indexOf('error-->'))
			} else {
				return JSON.parse(e.responseText).message;
			}			
		}catch(ex){
			try{
				if(e.indexOf('<!--error') >= 0 && e.indexOf('error-->') >= 0){
					return 	e.substring(e.indexOf('<!--error') + 9,e.indexOf('error-->'))
					
				}	
			}catch(ex1){
				//return '<spring:message code="lbl.session.expired"/>';
				return e.responseText;
			}
		}
		return "";
	}
	
	function getCurDate()
	{
		var date = new Date();
		var month = date.getMonth() + 1;
		return date.getFullYear() + '/' + (month > 9 ? month : "0" + month) + '/' + (date.getDate() > 9 ? date.getDate() : "0" + date.getDate()) + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(); 
	}
	
	function getProgress(initial, idImport)
	{
		 $.ajax({
		     url: "${contextPath}/import/progress",
		     type: "GET",
		     data: {
				initial: initial,
				idImport: idImport
		     },
		     cache: false,
		     success: function (data) {
				if(data.is_done != "true")
				{
					if(data.detail != "")
					{
						$('#errorMsg').append(getCurDate() + ": " + data.detail.replace(/(?:\r\n|\r|\n)/g, '<br>'));
						var elem = document.getElementById("errorMsg");
						elem.scrollTop = elem.scrollHeight;
					}
					$("#progress-bar").css({"width":data.message+ "%"});
					$("#progress-bar").html(data.message + "%");
					$('#status').html("Status: " + data.status);
					setTimeout(function() {
						getProgress('false', idImport);
					}, 1000);
				}
		     	
				
		     },
		     error: function (e) {
		        	
	        }
	   });
	}
	
	function makeId() {
		var text = Math.floor(Date.now() / 1000);
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		
		for (var i = 0; i < 10; i++)
	  		text += possible.charAt(Math.floor(Math.random() * possible.length));
		
		return text;
	}
	
	function showLoader()
	{
		$('#loading').modal('show');
	}
	
	function hideLoader()
	{
		setTimeout(function () {
	       	$('#loading').modal('hide');
		}, 300);
	}
	
	function errAjaxHandler(error){
	    showNotif(error.status, error.responseJSON.error);
	}

</script>