<!-- top navigation -->
<div class="top_nav">
	<div class="nav_menu">
    	<nav>
      		<div class="nav toggle">
        		<a id="menu_toggle"><i class="fa fa-bars"></i></a>
      		</div>
			
      		<ul class="nav navbar-nav navbar-right">
	        	<li class="">
	          		<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	            		<img src="../files/get-profile/{{Session::get('url_profile')}}" alt="">{{Session::get('username')}}
	            		<span class=" fa fa-angle-down"></span>
	          		</a>
	          		<ul class="dropdown-menu dropdown-usermenu pull-right">
	            		<li><a href="#" id="changePass">{{ __('password') }}</a></li>
	            		<li><a href="../login/logout"><i class="fa fa-sign-out pull-right"></i> {{ __('logout') }}</a></li>
	          		</ul>
	        	</li>
	        	<li class="">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    	<i id="currentLanguage" class="flag-icon"></i> <span id="labelLanguage">{{ app()->getLocale() }}</span>
                  	</a>
                  	<ul class="dropdown-menu dropdown-usermenu pull-right">
                  		<li onclick="changeLanguage('en')"><a><i class="flag-icon flag-icon-gb"></i> {{ __('english_lang') }}</a></li>
                  		<li onclick="changeLanguage('id')"><a><i class="flag-icon flag-icon-id"></i> {{ __('indonesian_lang') }}</a></li>
                  	</ul>
				</li>
      		</ul>
    	</nav>
	</div>
</div>
<!-- /top navigation -->

<!-- Modal Change Password -->
<div class="modal fade bs-example-modal-pass" id="modalChangePassword" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span></button>
			  	<h4 class="modal-title">Profile</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myFormPass">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Username</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='input' id="txtUsername" name="username" class="form-control" maxlength="30"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Firts Name</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='input' id="txtFirstName" name="firstName" class="form-control" maxlength="30"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Last Name</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='input' id="txtLastName" name="lastName" class="form-control" maxlength="30"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Email</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='input' id="txEmail" name="email" class="form-control" maxlength="30"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Photo</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input role="button" id="uploadProfile" class="input-file form-control" type="file" name="upload" accept="image/*">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Old Pass</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='password' id="txtOldPassword" name="oldPassword" class="form-control" maxlength="30"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">New Pass</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='password' id="txtNewPassword" name="newPassword" class="form-control" maxlength="30" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">Confirm Pass</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type='password' id="txtConfirmPassword" name="confirmPassword" class="form-control" maxlength="30"/>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnClosePass" class="btn btn-default">Close</button>
				<button type="button" id="btnSavePass" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Change Password -->
	<!-- Modal Loading -->
	<div class="modal fade bd-example-modal-lg" id="loading" data-backdrop="static" data-keyboard="false" tabindex="-1">
	    <div class="modal-dialog modal-md">
			<span class="fa fa-spinner fa-spin fa-3x fa-fw" style="color: #fff;font-size: 3.5em;"></span>
	    </div>
	</div>
<!-- Modal Loading -->