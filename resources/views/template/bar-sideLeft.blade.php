<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
    	<div class="navbar nav_title" style="border: 0;">
        	<a href="../home" class="site_title"><img src="../files/get-logo/{{Session::get('tenant_logo')}}" class="img img-responsive" style="margin:0 auto; width: 50px;height: 40px;display: inline-block;margin-right: 25px"/><span>Inventory</span></a>
		</div>

        <div class="clearfix"></div>

		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="../files/get-profile/{{Session::get('url_profile')}}" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>{{Session::get('username')}}</span>
                <h2>{{Session::get('full_name')}}</h2>
        	</div>
		</div>
		<!-- /menu profile quick info -->
        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        	<div class="menu_section">
                <!--<ul id="ul-main-menu" class="nav side-menu">
               		<li id="menu-1" style="display: block;">
               			<a href="../home"><i class="fa fa-home"></i>{{__('home')}}</a>
               		</li>
					<li id="menu-2" style="display: block;">
						<a><i class="fa fa-cog"></i>{{__('setting')}}<span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" id="ul-menu-2" style="">
							<li id="li-sub-menu-3"><a href="../setting/user" class="null"><span class="text">{{__('user')}}</span></a></li>
							<li id="li-sub-menu-4"><a href="../setting/role" class="null"><span class="text">{{__('role')}}</span></a></li>
						</ul>
					</li>
                </ul>-->
				@foreach(Session::get('menu') as $menu)
                <ul id="ul-main-menu" class="nav side-menu">
               		<!--<li style="display: block;">
               			<a href="{{ $menu->menu_path }}"><i class="{{ $menu->menu_icon }}"></i>{{ $menu->menu_name }}</a>
               		</li>-->
					@if(count($menu->sub_menu)==0)
						<li style="display: block;">
							<a href="{{ $menu->menu_path }}"><i class="{{ $menu->menu_icon }}"></i>{{ $menu->menu_name }}</a>
						</li>
					@endif
					@if(count($menu->sub_menu)>0)
						<li style="display: block;">
							<a><i class="{{ $menu->menu_icon }}"></i>{{ $menu->menu_name }}<span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								@foreach($menu->sub_menu as $sub_menu)
									<li><a href="{{ $sub_menu->menu_path }}" class="null"><span class="text">{{ $sub_menu->menu_name}}</span></a></li>
								@endforeach
							</ul>
						</li>
					@endif
                </ul>
				@endforeach
			</div>
		</div>
		<!-- /sidebar menu -->
	
	</div>
</div>