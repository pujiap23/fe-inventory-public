<!-- footer content -->
<footer>
	<div class="pull-left">
		Copyright <i class="fa fa-copyright" aria-hidden="true"></i> {{Session::get('year_now')}} PT. ZYP
	</div>
	<div class="pull-right">
		<!--${version.versionTitle}-->1.0.0
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->