<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	   	<title>Inventory - Login</title>
		<link rel="icon" href="../images/favicon.ico">

	    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">	
	    <link href="../vendors/custom/css/custom.min.css" rel="stylesheet">
		<link href="../vendors/pnotify/pnotify.custom.css" rel="stylesheet">

	</head>

  	<body class="login">
    	<div class="login_wrapper">
        	<section class="login_content">
            	<form class="form-horizontal">
              		<h1>Login Form</h1>
              		<div class="form-group">
                		<input id="txtUsername" type="text" class="form-control" name="username" placeholder="Username" required />
              		</div>
              		<div class="form-group">
                		<input id="txtPassword" type="password" class="form-control" name="password" placeholder="Password" required />
              		</div>
              		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
              		<div class="form-group">
                		<button id="btnLoginForm" type="button" class="btn btn-default">Login</button>
                	</div>
              		<div class="clearfix"></div>

              		<div class="separator">
	                	<div class="clearfix"></div>
	                	<br/>
	                	<div>
	                  		<div class="row">
								<div class="col-xs-12">
								</div>
							</div>
							<h1></h1>
	                  		<p>Copyright © 202 PT. A. Teknologi. Privacy and Terms</p>
	                	</div>
					</div>
            	</form>
			</section>
		</div>
		
		<script src="../vendors/jquery/dist/jquery.min.js"></script>
		<script src="../vendors/pnotify/pnotify.custom.js"></script>
		<script>
			var stack_bottomright = {"dir1": "right", "dir2": "up", "push": "top"};
			$("#txtUsername").focus();
			$("#btnLoginForm").on("click",function(){
				authValidations();
			});
			
			function authValidations(){
				var parameter={
		        		"mode":"WEBSITE",
						"username":$("#txtUsername").val(),
		        		"password":$("#txtPassword").val()
				};

				$.ajax({
		            type: "POST",
		            contentType: "application/json",
		            url: "../api/login",
		           	data:JSON.stringify(parameter),
		            dataType: 'json',
		            cache: false,
		            timeout: 600000,
		            success: function (data) {
		            	window.location.href="../home"
		            },
		            error: function (e) {
		            	if(e.status==0 || e.status==403 || e.status==500){
			    			showModal("", "Koneksi terputus")
		            	}
		            }
				});
			}

			function showNotif(type, message) {
				var typeStr = '';
				var title = '';
				if(type == '200' || type == '201'){
					typeStr = 'success';
					title = '<spring:message code="lbl.info"/>';
				}else if(type == '400' || type == '404'){
					typeStr = 'info';
					title = '<spring:message code="lbl.info"/>';
				}else{
					typeStr = 'error';
					title = '<spring:message code="lbl.error"/>';
				}
				var opts = {
					type: typeStr,
					title: title,
					text: message,
					delay: 2000,
					addclass: "stack-bottomright",
					stack: stack_bottomright
				};
				new PNotify(opts);
			}
		</script>
	</body>
</html>
