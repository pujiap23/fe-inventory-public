@include('template/head-open')
	<title>Inventory - 404</title>
	<link rel="icon" href="../images/favicon.ico">
	
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
	@include('template/head-close')
	@include('template/body-open')
	@include('template/div-open')
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="row">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<h1 class="error-number" style="color:#dd4b39 !important">404</h1>
						<h3><i class="fa fa-warning" style="color:#dd4b39 !important"></i> Oops ! This page is not available</h3>
						<p>
							<br>
							<a href="../home">Back to home</a>
						</p>
					</div>		
				</div>
			</div>
		</div>
	</div>
	@include('template/div-close')
	@include('template/core-js')
	@include('template/body-close')