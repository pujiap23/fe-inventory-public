<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">	
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
	<title>Inventory - Sales Billing List</title>
	<link rel="icon" href="../images/favicon.ico">
</head>
<body style="background:white;">
<div>
	<div>
	<!-- page content -->
	<div>
		<div class="" id="div-element-billing">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="">
						<div class="x_content">
							<form class="form-horizontal">
								<div class="form-group" style="color:black;" id="div-struct">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12 text-center">
													<h3 id="lblTenantName"><strong>{{Session::get('tenant')->tenant_name}}</strong></h3>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12 text-center">
													<label id="lblTenantName">{{Session::get('tenant')->tenant_address}}</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-4 col-sm-4 col-xs-4">
													<label id="lblStructCustomerName" class="control-label" style="padding-top:0px;">{{$billingDetails->customer_name}}</label>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-3 text-center">
													<label id="lblStructCreatedBy" class="control-label" style="padding-top:0px;">{{Session::get('username')}}</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5 text-right">
													<label id="lblStructBillingNumber" class="control-label" style="padding-top:0px;">{{$billingDetails->billing_number}}</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<label id="lblStructBillingDate" class="control-label" style="padding-top:0px;">{{$billingDetails->created_at}}</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row" id="detail-billing">
											@foreach($billingDetails->transaction_sub_sales_billing as $billingDetail)
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label class="control-label format-number" style="padding-top:0px;">{{$billingDetail->quantity}} X</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5">
													<label class="control-label" style="padding-top:0px;">{{$billingDetail->product_id->product_name}}</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5 text-right">
													<label class="control-label format-number" style="padding-top:0px;">{{$billingDetail->grant_total}}</label>
												</div>
											</div>
											@endforeach
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label" style="padding-top:0px;">Sub Total</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructSubTotal" class="control-label format-number" style="padding-top:0px;"><strong>{{$billingDetails->total_price}}</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label" style="padding-top:0px;">Discont</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructDiscount" class="control-label format-number" style="padding-top:0px;"><strong>{{$billingDetails->discount}}</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label" style="padding-top:0px;">Grant Total</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructGrantTotal" class="control-label format-number" style="padding-top:0px;"><strong>{{$billingDetails->grant_total}}</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label" style="padding-top:0px;">Payment</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructPayment" class="control-label format-number" style="padding-top:0px;"><strong>{{$billingDetails->total_payment}}</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label" style="padding-top:0px;">Change Payment</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructChangePayment" class="control-label format-number" style="padding-top:0px;"><strong>{{$billingDetails->change_payment}}</strong></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
	</div>
</div>
	<!-- jQuery -->
	<script src="../vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../build/plugin/paternNumber.js"></script>
	<script>
		/*var curr = document.getElementsByClassName("format-number").length;
		for(var a=0;a<curr;a++){
			document.getElementsByClassName("format-number")[a].innerHTML = document.getElementsByClassName("format-number")[a].innerHTML.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}*/
		change_to_currency();
		window.print();
	</script>
@include('template/body-close')