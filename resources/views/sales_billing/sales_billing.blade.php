@include('template/head-open')
	<title>Inventory - Sales Billing List</title>
	<link rel="icon" href="../images/favicon.ico">
	
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
@include('template/head-close')
@include('template/body-open')
@include('template/div-open')
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="" id="div-element-list">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('sales_billing')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="height: auto;">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input type='text' id="txtSearch" placeHolder="{{ __('lbl_search')}}" class="form-control" />
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnShow" class="btn btn-primary form-control">{{ __('lbl_show')}}</button>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnAdd" class="btn btn-success form-control"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;{{ __('lbl_add')}}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-bordered table-hover"  style="cursor: pointer;">
									<thead>
										<tr>
											<th class="sorting_table" value="0" hidden="true">{{ __('lbl_billing_id')}}</th>
											<th class="sorting_table" value="1">{{ __('lbl_billing_number')}}</th>
											<th class="sorting_table" value="2">{{ __('lbl_customer_name')}}</th>
											<th class="sorting_table" value="3">{{ __('lbl_total_product')}}</th>
											<th class="sorting_table" value="4">{{ __('lbl_discount')}}</th>
											<th class="sorting_table" value="5">{{ __('lbl_total_discount')}}</th>
											<th class="sorting_table" value="6">{{ __('lbl_total_price')}}</th>
											<th class="sorting_table" value="7">{{ __('lbl_grant_total')}}</th>
											<th class="sorting_table" value="8">{{ __('lbl_total_payment')}}</th>
											<th class="sorting_table" value="9">{{ __('lbl_change_payment')}}</th>
											<th class="sorting_table" value="10">{{ __('lbl_payment_status')}}</th>
											<th class="sorting_table" value="11">{{ __('lbl_warehouse')}}</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="" id="div-element-add" hidden="true">
			<div class="row" id="div-element-content">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('sales_billing')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<form class="form-horizontal">
								<div class="form-group">
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input type='text' id="txtCustomerName" class="form-control" placeHolder="{{__('lbl_customer_name')}}" />
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input type='text' id="txtBillingDate" class="form-control" readonly/>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<input type='text' id="txtCreatedBy" class="form-control" value="{{Session::get('username')}}" readonly />
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-8 col-sm-8 col-xs-12">
										<select class="form-control" id="cmbProduct">
											<option value="">{{__('lbl_choose')}}</option>
										</select>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-12">
										<input type='text' style="height: 38px;" id="txtQuantity" class="form-control number" value="1" placeHolder="{{__('lbl_quantity')}}"/>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-12">
										<button type="button" style="height: 38px;" id="buttonAddProduct" class="btn btn-success form-control" data-dismiss="modal"><i class="fa fa-plus"></i>&nbsp;&nbsp;{{__('lbl_add')}}</button>
									</div>
								</div>
								<div class="form-group" id="divTransactionStatus">
									<!--<div class="col-md-4 col-sm-4 col-xs-12">
										<button type="button" style="height: 38px;" id="buttonCash" class="btn btn-info form-control payment-method" onclick="choosePaymentMethod('buttonCash')">&nbsp;&nbsp;{{__('lbl_cash')}}</button>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<button type="button" style="height: 38px;" id="buttonCard" class="btn btn-info form-control payment-method" onclick="choosePaymentMethod('buttonCard')">&nbsp;&nbsp;{{__('lbl_card')}}</button>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-12">
										<button type="button" style="height: 38px;" id="buttonPituang" class="btn btn-info form-control payment-method" onclick="choosePaymentMethod('buttonPituang')">&nbsp;&nbsp;{{__('lbl_pituang')}}</button>
									</div>-->
								</div>
								<div class="form-group">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="table-responsive">
											<table id="datatable-product" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="text-center" width="10%" value="1">{{ __('lbl_product_name')}}</th>
														<th class="text-center" width="10%" value="2">{{ __('lbl_product_code')}}</th>
														<th class="text-right" width="15%" value="3">{{ __('lbl_selling_price')}}</th>
														<th class="text-right" width="20%" value="4">{{ __('lbl_discount')}}</th>
														<th class="text-right" width="10%" value="5">{{ __('lbl_quantity')}}</th>
														<th class="text-right" width="20%" value="6">{{ __('lbl_grant_total')}}</th>
														<th class="text-center" width="10%" value="7">{{ __('lbl_delete')}}</th>
													</tr>
												</thead>
											<tbody id="body-table-product"></tbody>
											<tfoot id="footer-table-product">
													<tr id="tr-sub-total" style="background-color: #eee;color: black;font-size: medium;">
														<td class="text-center" colspan="5">{{__('lbl_sub_total')}}</td>
														<td class="text-right" id="lbl-sub-total">0</td>
														<td class="text-center"></td>
													</tr>
													<tr id="tr-discount" style="background-color: #eee;color: black;font-size: medium;">
														<td class="text-center" colspan="5">{{__('lbl_discount')}}</td>
														<td class="text-right" id="lbl-sub-discount">0</td>
														<td class="text-center"></td>
													</tr>
													<tr id="tr-grant-total" style="background-color: #eee;color: black;font-size: medium;">
														<td class="text-center" colspan="5"><strong>{{__('lbl_grant_total')}}</strong></td>
														<td class="text-right" id="lbl-grant-total">0</td>
														<td class="text-center"></td>
													</tr>
													<tr id="tr-grant-total" style="background-color: #eee;color: black;font-size: medium;" >
														<td class="text-center" colspan="5">{{__('lbl_total_payment')}}</td>
														<td class="text-right"><input style="border: 0px;background: #eee;padding-right: 0px;margin: 0 0 0 0;" type="text" id="txtPayment" class="number text-right" placeHolder="0"/></td>
														<td class="text-center"><input type="button" style="margin:0 0 0 0;" onclick="exact()" id="buttonExact" class="btn btn-info form-control" value="{{__('lbl_exact')}}"/></td>
													</tr>
													<tr id="tr-chenge" style="background-color: #eee;color: black;font-size: medium;">
														<td class="text-center" colspan="5">{{__('lbl_change_payment')}}</td>
														<td class="text-right" id="lbl-change-payment">0</td>
														<td class="text-center"></td>;
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<div class="form-group" id="div-element-button">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<button type="button" style="height: 38px;" id="buttonCancel" class="btn btn-danger form-control"><i class="fa fa-close"></i>&nbsp;&nbsp;{{__('lbl_cancel')}}</button>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<button type="button" style="height: 38px;" id="buttonSubmit" class="btn btn-success form-control"><i class="fa fa-save"></i>&nbsp;&nbsp;{{__('lbl_save')}}</button>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<button type="button" style="height: 38px;" id="buttonPrint" class="btn btn-info form-control"><i class="fa fa-print"></i>&nbsp;&nbsp;{{__('lbl_print')}}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--<div class="" id="div-element-billing" hidden="true">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('sales_billing')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<form class="form-horizontal">
								<div class="form-group" style="color:black;" id="div-struct">
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12 text-center">
													<h3 id="lblTenantName"><strong>{{Session::get('tenant')->tenant_name}}</strong></h3>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-12 text-center">
													<label id="lblTenantName">{{Session::get('tenant')->tenant_address}}</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-4 col-sm-4 col-xs-4">
													<label id="lblStructCustomerName" class="control-label">Customer Name</label>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-4 text-center">
													<label id="lblStructCreatedBy" class="control-label">{{Session::get('username')}}</label>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-4 text-right">
													<label id="lblStructBillingNumber" class="control-label">20211026SB001</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<label id="lblStructBillingDate" class="control-label">23 oct 2021 00:00:00</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row" id="detail-billing">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label class="control-label">1 X</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5">
													<label class="control-label">Karpet</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5 text-right">
													<label class="control-label">12,000</label>
												</div>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label class="control-label">1 X</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5">
													<label class="control-label">Karpet batu batu</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5 text-right">
													<label class="control-label">100,000,000</label>
												</div>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label class="control-label">10 X</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5">
													<label class="control-label">Batu bata</label>
												</div>
												<div class="col-md-5 col-sm-5 col-xs-5 text-right">
													<label class="control-label">12,000,000</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<hr style="border-style: dashed;border-color:black;margin: 0 0 0 0;">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label">Sub Total</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructSubTotal" class="control-label"><strong>112,012,000</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label">Discont</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructDiscount" class="control-label"><strong>0</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label">Grant Total</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructGrantTotal" class="control-label"><strong>112,012,000</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label">Payment</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructPayment" class="control-label"><strong>112,012,000</strong></label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-5 col-sm-5 col-xs-6">
													<label class="control-label">Change Payment</label>
												</div>
												<div class="col-md-7 col-sm-7 col-xs-6 text-right">
													<label id="lblStructChangePayment" class="control-label"><strong>0</strong></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<form class="form-horizontal">
								<div class="form-group">
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="col-md-6 col-sm-6 col-xs-6 text-right">
													<button type="button" style="height: 38px;" id="buttonCancelStruck" class="btn btn-danger form-control"><i class="fa fa-close"></i>&nbsp;&nbsp;{{__('lbl_cancel')}}</button>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6">
													<button type="button" style="height: 38px;" id="buttonPrintStruck" class="btn btn-success form-control"><i class="fa fa-print"></i>&nbsp;&nbsp;{{__('lbl_print')}}</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>-->
	</div>
	<!-- /page content -->
@include('template/div-close')
@include('template/core-js')
<!-- Datatables -->
	<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="../build/js/jquery.spring-friendly.js"></script>
	<script>
		var status_transaction = "add";
		var grand_total = 0;
		var grand_discount = 0;
		var transaction_status_id = 1;
		var sub_product = [];
		var number = 0;
		var this_id = null;
		
		$(document).ready(function() {
			datatable();
			getAllProduct();
			getAllTransactionStatus();
		});
		
		$("#cmbProduct").select2();
		
		$('#btnAdd').click(function () {
			status_transaction = "add";
			date = new Date();
			new_date =  date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
			$('#div-element-list').attr('hidden',true);
			$('#div-element-add').attr('hidden',false);
			$("#txtBillingDate").val(new_date);
			$("#buttonPrint").attr('disabled',true);
		});
		
		$('#buttonCancelStruck').click(function(){
			$('#div-element-list').attr('hidden',false);
			$('#div-element-add').attr('hidden',true);
			$('#div-element-billing').attr('hidden',true);
		});
		
		$('#buttonCancel').click(function () {
			clear_form();
			$('#div-element-list').attr('hidden',false);
			$('#div-element-add').attr('hidden',true);
		});
		
		$('#btnShow').click(function () {
			datatable();
		});
		
		$('#buttonAddProduct').click(function () {
			addProduct();
		});
		
		$('#buttonSubmit').click(function () {
			if(sub_product.length > 0 && Number($("#txtPayment").val().replace(/,/g, "")) > 0){
				if (status_transaction=="add"){
					saveTransaction();
				}else if (status_transaction=="edit"){
					status_transaction="update";
					$("#buttonPrint").attr("disabled", true);
					$("#div-element-content *").attr("disabled", false);
					document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-save'></i>&nbsp;&nbsp;{{__('lbl_update')}}";
				}else{
					updateTransaction();
				}
			}else{
				showNotif('', '{{__("msg_validate_sales_billing")}}');
			}
		});
		
		$("#txtPayment").on('keyup', function(){
			 setChangePayment();
		});
		
		$("#txtQuantity").on("keyup", function(e){
			if(e.keyCode==13){
				addProduct();
			}
		});
		
		$("#buttonPrint").click(function(e){
			printStruck(this_id);
		});
		
		$("#txtSearch").on("keyup", function(e){
			if(e.keyCode==13){
				datatable();
			}
		});
		
        function printStruck(id){
            //var divContents = document.getElementById("div-struct").innerHTML;
            var a = window.open('../transaction/struck?billing_id=' + id, '', 'height=1000, width=500');
            //a.print();
        }
		
		function addProduct(){
			var qty = Number($("#txtQuantity").val());
			var product = $("#cmbProduct").val();
			if(Number(qty) > 0  && product!=""){
				getAndSetProduct();
			}else{
				if (Number(qty) == 0){
					$("#txtQuantity").focus();
					showNotif('400', '{{__("msg_required_quantity")}}');
				}else{
					$("#cmbProduct").focus();
					showNotif('400', '{{__("msg_required_selected_product")}}');
				}
			}
		}
		
		function exact(){
			$("#txtPayment").val((grand_total-grand_discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			setChangePayment();
		}
		
		function choosePaymentMethod(id){
			transaction_status_id = $("#"+id).val();
			var elementother = document.getElementsByClassName("payment-method");
			for(var a=0;a<elementother.length;a++){
				if(id!=elementother[a].id){
					elementother[a].classList.remove("btn-primary");
					elementother[a].classList.add("btn-info");
				}else{
					elementother[a].classList.remove("btn-info");
					elementother[a].classList.add("btn-primary");
				}
			}
			$("#txtPayment").focus();
		}
		
		function saveTransaction(){
			data = {
				"_token": "{{ csrf_token() }}",
                'customer_id'    			: null,
                'customer_name'    			: $('#txtCustomerName').val(),
                'transaction_status_id'     : transaction_status_id,
                'total_product'   			: 0,
                'discount'   				: 0,
                'total_discount'   			: 0,
                'total_price'   			: 0,
                'grant_total'   			: 0,
                'total_payment'   			: Number($('#txtPayment').val().replace(/,/g, "")),
                'change_payment'   			: 0,
                'description'   			: "",
                'sub_sales_billing'     	: sub_product
			}
			
			$.ajax({
				type : "POST",
				url : "../transaction/sales-billing/create",
				timeout: 10000,
				accept:'application/json',
				data:JSON.stringify(data),
				contentType:'application/json',
				success : function(data) {
					clear_form();
					$('#div-element-list').attr('hidden',false);
					$('#div-element-add').attr('hidden',true);
					//$('#div-element-billing').attr('hidden',false);
					showNotif('200', '{{__("msg_success_save_sales_billing")}}' + " " + data.sales_billing.billing_number);
					printStruck(data.sales_billing.id);
					datatable();
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function updateTransaction(){
			data = {
				"_token": "{{ csrf_token() }}",
                'customer_id'    			: null,
                'customer_name'    			: $('#txtCustomerName').val(),
                'transaction_status_id'     : transaction_status_id,
                'total_product'   			: 0,
                'discount'   				: 0,
                'total_discount'   			: 0,
                'total_price'   			: 0,
                'grant_total'   			: 0,
                'total_payment'   			: Number($('#txtPayment').val().replace(/,/g, "")),
                'change_payment'   			: 0,
                'description'   			: "",
                'sub_sales_billing'     	: sub_product
			}
			
			$.ajax({
				type : "POST",
				url : "../transaction/sales-billing/update/"+this_id,
				timeout: 10000,
				accept:'application/json',
				data:JSON.stringify(data),
				contentType:'application/json',
				success : function(data) {
					clear_form();
					$('#div-element-list').attr('hidden',false);
					$('#div-element-add').attr('hidden',true);
					//$('#div-element-billing').attr('hidden',false);
					showNotif('200', '{{__("msg_success_update_sales_billing")}}' + " " + data.sales_billing.billing_number);
					printStruck(data.sales_billing.id);
					datatable();
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function clear_form(){
			grand_total = 0;
			grand_discount = 0;
			for (var a=0;a<sub_product.length;a++){
				var trelement = document.getElementById("tr-"+sub_product[a].initial);
				if(trelement){
					trelement.remove();
				}
			}
			sub_product = [];
			$("#txtPayment").val("0");
			$("#txtQuantity").val("1");
			$("#txtCustomerName").val("");
			$("#txtCreatedBy").val("{{Session::get('username')}}");
			$("#cmbProduct").val("");
			$("#cmbProduct").trigger('change');
			$("#div-element-content *").attr("disabled", false);
			document.getElementById("lbl-sub-total").innerHTML = "0";
			document.getElementById("lbl-grant-total").innerHTML = "0";
			document.getElementById("lbl-change-payment").innerHTML = "0";
			document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-save'></i>&nbsp;&nbsp;{{__('lbl_save')}}";
			$("#txtPayment").val(0);
			number = 0;
		}
		
		function getAndSetProduct(){
			$.ajax({
				type : "GET",
				url : "../product-detail/"+$("#cmbProduct").val(),
				timeout: 4000,
				success : function(data) {
					var str = "";
					number = number + 1;
					qty = $("#txtQuantity").val();
					total = (Number(data.selling_price)) * (Number(qty));
					str += '<tr id="tr-'+number+'">';
					str += '<td class="text-center">'+data.product_name+'</td>';
					str += '<td class="text-center">'+data.product_code+'</td>';
					str += '<td class="text-right">'+data.selling_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</td>';
					str += '<td class="text-right" id="grant-discount-'+number+'">'+data.discount_persentage+'%</td>';
					str += '<td class="text-center">'+$("#txtQuantity").val()+'</td>';
					str += '<td class="text-right" id="grant-total-'+number+'">'+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</td>';
					str += '<td class="text-center"><button type="button" onclick="deleteData('+number+')" title="{{ __("lbl_delete")}}" data-id="'+ number +'" class="btn btn-xs btn-delete delete-product" style="margin-bottom:0px;background-color:white;color: black;border: 2px solid #e7e7e7;" data-toggle="modal" data-target="#myModal"><a><i class="fa fa-fw fa-trash" style="color: red;"></i></a></button></td>';
					str += '</tr>';
					//grand_total = grand_total + Number(data.selling_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
					grand_discount = grand_discount + ((Number(qty) * Number(data.selling_price)) * (Number(data.discount_persentage)/100));
					grand_total = grand_total + total;

					//document.getElementById("body-table-product").innerHTML = str;
					$("#body-table-product").append(str);
					
					document.getElementById("lbl-sub-total").innerHTML = grand_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					document.getElementById("lbl-sub-discount").innerHTML = grand_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					document.getElementById("lbl-grant-total").innerHTML = '<strong>' + (grand_total-grand_discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</strong>';
					document.getElementById("lbl-change-payment").innerHTML = "0";
					
					$("#txtPayment").focus();
					setChangePayment();
					
					var arr_product = {
										"id":null,
										"initial":number,
										"product_id":data.id,
                                        "quantity":Number(qty),
                                        "discount":0,
                                        "total_discount":0,
                                        "total_price":(Number(data.selling_price)),
                                        "grant_total":total
									};
					sub_product.push(arr_product);
				},
				error : function(textStatus) {
					loadOff();
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getAllProduct(){
			$.ajax({
				type : "GET",
				url : "../product-dropdown-list",
				timeout: 100000,
				data: {
						search:""					
				},
				success : function(data) {
					for(var a=0;a<data.length;a++){
						$("#cmbProduct").append("<option value='"+data[a].id+"'>"+data[a].product_code + " - " + data[a].product_name+"</option>")
					}
				},
				error : function(textStatus) {
					loadOff();
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getSearchProduct(dataSearch){
			$.ajax({
				type : "GET",
				url : "../product-list",
				data: {
						search:dataSearch
					},
				timeout: 4000,
				success : function(data) {
					/*for(var a=0;a<data.data.length;a++){
						$("#cmbProduct").append("<option value='"+data.data[a].id+"'>"+data.data[a].product_code + " - " + data.data[a].product_name+"</option>")
					}*/
					return data.data;
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function getAllTransactionStatus(){
			$.ajax({
				type : "GET",
				url : "../transaction-status-list",
				timeout: 100000,
				success : function(data) {
					var setCol = "";
					if (data.recordsTotal==1){
						setCol = "col-md-12 col-sm-12 col-xs-12"
					}
					if (data.recordsTotal==2){
						setCol = "col-md-6 col-sm-6 col-xs-12"
					}
					if (data.recordsTotal==3){
						setCol = "col-md-4 col-sm-4 col-xs-12"
					}
					if (data.recordsTotal==4){
						setCol = "col-md-3 col-sm-3 col-xs-12"
					}
					if (data.recordsTotal==5){
						setCol = "col-md-3 col-sm-3 col-xs-12"
					}
					if (data.recordsTotal==6){
						setCol = "col-md-2 col-sm-2 col-xs-12"
					}
					for(var a=0;a<data.data.length;a++){
						$("#divTransactionStatus").append("<div class='"+setCol+"'><button value='"+data.data[a].id+"' type='button' style='height: 38px;' id='button"+data.data[a].status_name+ "'class='btn btn-info form-control payment-method' onclick='choosePaymentMethod(\"button"+data.data[a].status_name+"\")'>&nbsp;&nbsp;"+data.data[a].status_name+"</button></div>")
					}
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function setChangePayment(){
			payment_str = $("#txtPayment").val();
			payment = Number(payment_str.toString().replace(/,/g, ""));
			$("#txtPayment").val(payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
			document.getElementById("lbl-change-payment").innerHTML = (payment - (grand_total-grand_discount)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		
		function deleteData(id) {
			var subtotal = document.getElementById("grant-total-"+id);
			var subdisc = document.getElementById("grant-discount-"+id);
			var tmp_grant_total = Number(subtotal.innerHTML.toString().replace(/,/g, ""));
			var tmp_grant_disc = Number(subdisc.innerHTML.toString().replace(/,/g, "").replace('%',"")) / 100;
			grand_total = grand_total-tmp_grant_total;
			grand_discount = grand_discount-(tmp_grant_disc*tmp_grant_total);
			
			document.getElementById("lbl-sub-total").innerHTML = grand_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			document.getElementById("lbl-sub-discount").innerHTML = grand_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			if(grand_discount<=0){
				document.getElementById("tr-discount").style.display = "none";
			}
			document.getElementById("lbl-grant-total").innerHTML = '<strong>' + (grand_total-grand_discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")  + '</strong>';
			setChangePayment();

			if (status_transaction == "add"){
				var myobj = document.getElementById("tr-"+id);
				myobj.remove();
				const dataRemoved = sub_product.filter((el) => {
					return el.initial !== id;
				});
				sub_product = dataRemoved;
			}else{
				var myobj = document.getElementById("tr-"+id);
				document.getElementById("quantity-"+id).innerHTML = 0;
				document.getElementById("grant-total-"+id).innerHTML = 0;
				const dataRemoved = sub_product.filter((el) => {
					if (el.initial == id){
						el.quantity = 0;
					}
					return el;
				});
				sub_product = dataRemoved;
			}
		}
		
		function get_detail(id){
			$('#div-element-list').attr('hidden',true);
			$('#div-element-add').attr('hidden',false);
			$("#buttonPrint").attr('disabled',false);
			
			$("#div-element-content *").attr("disabled", "disabled").off('click');
			document.getElementById("buttonSubmit").innerHTML = "<i class='fa fa-pencil'></i>&nbsp;&nbsp;{{__('lbl_edit')}}";
			//$("#buttonSubmit").val("&nbsp;&nbsp;{{__('lbl_print')}}");
			
			$.ajax({
				type : "GET",
				url : "../sales-billing/" + id,
				timeout: 100000,
				success : function(data) {
					document.getElementById("body-table-product").innerHTML = "";
					sub_product = [];
					number = 0;
					grand_discount = 0;
					grand_total = 0;
					this_id = id;
					var str = "";
					for(var a=0;a<data.transaction_sub_sales_billing.length;a++){
						number = number + 1;
						qty = data.transaction_sub_sales_billing[a].quantity;
						total = (Number(data.transaction_sub_sales_billing[a].product_id.selling_price)) * (Number(qty));
						str += '<tr id="tr-'+number+'">';
						str += '<td class="text-center">'+data.transaction_sub_sales_billing[a].product_id.product_name+'</td>';
						str += '<td class="text-center">'+data.transaction_sub_sales_billing[a].product_id.product_code+'</td>';
						str += '<td class="text-right">'+data.transaction_sub_sales_billing[a].product_id.selling_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</td>';
						str += '<td class="text-right" id="grant-discount-'+number+'">'+data.transaction_sub_sales_billing[a].product_id.discount_persentage+'%</td>';
						str += '<td class="text-right" id="quantity-'+number+'">'+qty+'</td>';
						str += '<td class="text-right" id="grant-total-'+number+'">'+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</td>';
						str += '<td class="text-center"><button type="button" disabled onclick="deleteData('+number+')" title="{{ __("lbl_delete")}}" data-id="'+ number +'" class="btn btn-xs btn-delete delete-product" style="margin-bottom:0px;background-color:white;color: black;border: 2px solid #e7e7e7;" data-toggle="modal" data-target="#myModal"><a><i class="fa fa-fw fa-trash" style="color: red;"></i></a></button></td>';
						str += '</tr>';
						//grand_total = grand_total + Number(data.selling_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
						grand_discount = grand_discount + ((Number(qty) * Number(data.transaction_sub_sales_billing[a].product_id.selling_price)) * (Number(data.transaction_sub_sales_billing[a].product_id.discount_persentage)/100));
						grand_total = grand_total + total;
						
						var arr_product = {
											"initial":number,
											"id":data.transaction_sub_sales_billing[a].id,
											"transaction_id":data.id,
											"product_id":data.transaction_sub_sales_billing[a].product_id.id,
											"quantity":Number(qty),
											"discount":0,
											"total_discount":0,
											"total_price":(Number(data.transaction_sub_sales_billing[a].product_id.selling_price)),
											"grant_total":total
										};
						sub_product.push(arr_product);
					}

					//document.getElementById("body-table-product").innerHTML = str;
					$("#body-table-product").append(str);
					
					document.getElementById("lbl-sub-total").innerHTML = grand_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					document.getElementById("lbl-sub-discount").innerHTML = grand_discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					document.getElementById("lbl-grant-total").innerHTML = '<strong>' + (grand_total-grand_discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</strong>';
					$("#txtPayment").val(data.total_payment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$("#txtBillingDate").val(data.created_at);
					$("#txtCustomerName").val(data.customer_name);
					$("#txtCreatedBy").val(data.created_by.username);
					$("#txtPayment").focus();
					choosePaymentMethod("button"+data.transaction_status_id.status_name);
					$("#button"+data.transaction_status_id.status_name).attr("disabled", false);
					setChangePayment();
				},
				error : function(textStatus) {
					errAjaxHandler(textStatus);
				},
			});
		}
		
		function datatable(){
			var table = $('#datatable').DataTable({
				'ajax':
				{
					url:'../sales-billing-list',
					data: {
						search:$('#txtSearch').val()
					},
					error: function (e) {
						NProgress.done();
						showNotif(e.status, e.responseJSON.error.message);
					}
				},
				'paging'      : true,
			    'lengthChange': false,
			    'destroy'	  : true,
			    'searching'   : false,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : false,
			    'processing'  : true,
			    'serverSide'  : true,
				'order': [[ 0, "desc" ]],
				'columns'     : [
				{
					data : 'id',
					'visible' : false
				}, {
					data : 'billing_number'
				}, {
					data : 'customer_name'
				}, {
					data : 'total_product'
				}, {
					data : 'discount',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "%";}
					}
				}, {
					data : 'total_discount',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return "Rp" + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
					}
				}, {
					data : 'total_price',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return "Rp" + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
					}
				}, {
					data : 'grant_total',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return "Rp" + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
					}
				}, {
					data : 'total_payment',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return "Rp" + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
					}
				}, {
					data : 'change_payment',
					className : 'text-right',
					'render' : function(data){ if(data==null){return data=='0';}else{
						return "Rp" + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
					}
				}, {
					data : 'status_name'
				}, {
					data : 'warehouse_name'
				}]
			}).on('click', 'tbody tr', function () {
				var data = table.row(this).data();
				status_transaction = "edit";
				get_detail(data.id);
			});
		}
	</script>
@include('template/body-close')