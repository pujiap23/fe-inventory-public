@include('template/head-open')
	<title>Inventory - Product Type List</title>
	<link rel="icon" href="../images/favicon.ico">
	
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
@include('template/head-close')
@include('template/body-open')
@include('template/div-open')
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>{{ __('product-type')}}</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content" style="height: auto;display: none">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="form-group">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<input type='text' id="txtSearch" placeHolder="{{ __('lbl_search')}}" class="form-control" />
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnShow" class="btn btn-primary form-control">{{ __('lbl_show')}}</button>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12">
												<button type="button" id="btnAdd" class="btn btn-success form-control"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;{{ __('lbl_add')}}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
                          				<th class="sorting_table" value="0" hidden="true">{{ __('lbl_product_type_id')}}</th>
                          				<th class="sorting_table" value="1">{{ __('lbl_name')}}</th>
										<th class="sorting_table" value="2">{{ __('lbl_description')}}</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
	
	
	<div class="modal fade bs-example-modal-md" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
				  	<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group" hidden="true">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_user_id')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='text' id="txtUserIdPopup" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_first_name')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='text' id="txtFirstNamePopup" class="form-control" maxlength="30"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_last_name')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='text' id="txtLastNamePopup" class="form-control" maxlength="30"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_username')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='text' id="txtUsernamePopup" class="form-control" maxlength="30"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_email')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='text' id="txtEmailPopup" class="form-control" maxlength="30"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_password')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='password' id="txtPasswordPopup" class="form-control" maxlength="255"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_confirm_password')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type='password' id="txtConfirmPasswordPopup" class="form-control form-control-lg" maxlength="255"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="text-align:left !important">{{__('lbl_role')}}</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="form-control" id="cmbRolePopup">
									<option value="">{{__('lbl_role')}}</option>
									<!--<c:forEach items="${roleList}" var="roleList">
										<option value="${roleList.roleId}">${fn:escapeXml(roleList.roleId)} - ${fn:escapeXml(roleList.roleName)}</option>
									</c:forEach>-->
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{__('lbl_close')}}</button>
					<button type="button" id="btnDelete" class="btn btn-danger">{{__('lbl_delete')}}</button>
					<button type="button" id="btnSave" data-action="" class="btn btn-primary">{{__('lbl_save')}}</button>
				</div>
			</div>
		</div>
	</div>
	<!-- page content -->
@include('template/div-close')
@include('template/core-js')
<!-- Datatables -->
	<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="../build/js/jquery.spring-friendly.js"></script>
	<script>
		$(document).ready(function() {
			datatable();
		});
		
		$('#btnAdd').click(function () {
			$('.bs-example-modal-md').modal('show');
		});
		
		$('#btnShow').click(function () {
			datatable();
		});
			
		function datatable(){
			var table = $('#datatable').DataTable({
				'ajax':
				{
					url:'../product-type-list',
					data: {
						search:$('#txtSearch').val()
					},
					error: function (e) {
						NProgress.done();
						showNotif(e.status, e.responseJSON.error.message);
					}
				},
				'paging'      : true,
			    'lengthChange': false,
			    'destroy'	  : true,
			    'searching'   : false,
			    'ordering'    : true,
			    'info'        : true,
			    'autoWidth'   : false,
			    'processing'  : true,
			    'serverSide'  : true,
				'columns'     : [
				{
					data : 'id',
					'visible' : false
				}, {
					data : 'type_name'
				}, {
					data : 'description'
				}]
			});
		}
	</script>
@include('template/body-close')